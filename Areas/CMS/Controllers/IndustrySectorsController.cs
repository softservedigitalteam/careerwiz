﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.IndustrySectors;
using CareerWizCMS.Model_Manager;
using CareerWiz;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using System.Drawing;
using System.IO;

namespace CareerWizCMS.Controllers
{
    public class IndustrySectorsController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: IndustrySectors
        public ActionResult IndustrySectorsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorsView clsIndustrySectorsView = new clsIndustrySectorsView();
            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsIndustrySectorsView.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();

            return View(clsIndustrySectorsView);
        }

        public ActionResult IndustrySectorAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectors clsIndustrySector = new clsIndustrySectors();

            return View(clsIndustrySector);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IndustrySectorAdd(clsIndustrySectors clsIndustrySector)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsIndustrySectorsManager.saveIndustrySector(clsIndustrySector);

            //Add successful / notification
            TempData["bIsIndustrySectorAdded"] = true;

            return RedirectToAction("IndustrySectorsView", "IndustrySectors");
        }

        //Edit CMS Page
        public ActionResult IndustrySectorEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("IndustrySectorsView", "IndustrySectors");
            }

            clsIndustrySectorEdit clsIndustrySectorEdit = new clsIndustrySectorEdit();
            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();

            clsIndustrySectorEdit.clsIndustrySector = clsIndustrySectorsManager.getIndustrySectorByID(id);
            clsIndustrySectorEdit.strTitle = clsIndustrySectorEdit.clsIndustrySector.strTitle;

            return View(clsIndustrySectorEdit);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IndustrySectorEdit(clsIndustrySectorEdit clsIndustrySectorEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsIndustrySectors clsExistingIndustrySector = clsIndustrySectorsManager.getIndustrySectorByID(clsIndustrySectorEdit.clsIndustrySector.iIndustrySectorID);

            //clsExistingIndustrySector.strTitle = clsIndustrySector.strTitle;
            clsExistingIndustrySector.strDescription = clsIndustrySectorEdit.clsIndustrySector.strDescription;
            clsIndustrySectorsManager.saveIndustrySector(clsExistingIndustrySector);

            //Add successful / notification
            TempData["bIsIndustrySectorUpdated"] = true;

            return RedirectToAction("IndustrySectorsView", "IndustrySectors");
        }

        //Delete Page
        [HttpPost]
        public ActionResult IndustrySectorDelete(int iIndustrySectorID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iIndustrySectorID == 0)
            {
                return RedirectToAction("IndustrySectorsView", "IndustrySectors");
            }

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsIndustrySectorsManager.removeIndustrySectorByID(iIndustrySectorID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsIndustrySectorDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfIndustrySectorExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblIndustrySectors.Any(IndustrySector => IndustrySector.strTitle.ToLower() == strTitle.ToLower() && IndustrySector.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult IndustrySectorsImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IndustrySectorsImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("IndustrySectorsImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();

            List<clsIndustrySectors> lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to IndustrySectors Table
                        clsIndustrySectors clsIndustrySector = new clsIndustrySectors();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exIndustrySectorTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //IndustrySector Title
                                if (exIndustrySectorTitle == null || exIndustrySectorTitle == "")
                                    continue;
                                if (lstIndustrySectors.Any(IndustrySector => IndustrySector.strTitle.ToLower() == exIndustrySectorTitle.ToLower()))
                                    continue;

                                clsIndustrySector.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //IndustrySector Title
                                clsIndustrySector.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //IndustrySector Description

                                //lstIndustrySectors.Add(clsIndustrySector);
                                clsIndustrySectorsManager.saveIndustrySector(clsIndustrySector);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("IndustrySectorsImportError", "Could not import IndustrySectors. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsIndustrySectorsImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to IndustrySectors Table
                        clsIndustrySectors clsIndustrySector = new clsIndustrySectors();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exIndustrySectorTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //IndustrySector Title
                                if (exIndustrySectorTitle == null || exIndustrySectorTitle == "")
                                    continue;
                                if (lstIndustrySectors.Any(IndustrySector => IndustrySector.strTitle.ToLower() == exIndustrySectorTitle.ToLower()))
                                    continue;

                                clsIndustrySector.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //IndustrySector Title
                                clsIndustrySector.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //IndustrySector Description

                                //lstIndustrySectors.Add(clsIndustrySector);
                                clsIndustrySectorsManager.saveIndustrySector(clsIndustrySector);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("IndustrySectorsImportError", "Could not import IndustrySectors. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsIndustrySectorsImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("IndustrySectorsImportError", "Could not import IndustrySectors. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("IndustrySectorsView", "IndustrySectors");
        }
    }
}