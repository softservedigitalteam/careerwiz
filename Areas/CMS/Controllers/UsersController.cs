﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Model_Manager;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models;
using CareerWizCMS.View_Models.Users;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CareerWizCMS.Assistant_Classes;

namespace CareerWizCMS.Controllers
{
    public class UsersController : Controller
    {
        // GET: User

        //Get All Users
        public ActionResult UsersView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsUsersView clsUsersView = new clsUsersView();
            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsersView.lstUsers = clsUsersManager.getAllUsersList();

            return View(clsUsersView);
        }

        //Add User
        public ActionResult UserAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsUserAdd clsUserAdd = new clsUserAdd();
            clsRoleTypesManager clsRoleTypesManager = new clsRoleTypesManager();
            clsUserAdd.lstRoleTypes = clsRoleTypesManager.getAllRoleTypesList();

            return View(clsUserAdd);
        }

        //Add User
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserAdd(clsUserAdd clsUserAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsStudents clsStudent = null;

            //Add student data
            clsStudentsManager clsStudentsManager = new clsStudentsManager();
            clsStudent = new clsStudents();

            DateTime dtDateOfBirth = new DateTime((int)clsUserAdd.iDateOfBirthYear, (int)clsUserAdd.iDateOfBirthMonth, (int)clsUserAdd.iDateOfBirthDay);

            clsStudent.dtDateOfBirth = dtDateOfBirth;
            clsStudent.bIsPaidSubscription = clsUserAdd.clsUser.clsStudent.bIsPaidSubscription;
            clsStudent.strGender = clsUserAdd.clsUser.clsStudent.strGender;
            clsStudent.strNationality = clsUserAdd.clsUser.clsStudent.strNationality;
            clsStudent.strRegion = clsUserAdd.clsUser.clsStudent.strRegion;
            clsStudent.bHigherEducation = clsUserAdd.clsUser.clsStudent.bHigherEducation;
            clsStudent.strInstitutionName = clsUserAdd.clsUser.clsStudent.strInstitutionName;
            clsStudent.strCourse = clsUserAdd.clsUser.clsStudent.strCourse;
            clsStudent.strModules = clsUserAdd.clsUser.clsStudent.strModules;

            clsUserAdd.clsUser.iStudentID = clsStudentsManager.saveStudent(clsStudent);
            clsUserAdd.clsUser.bIsStudent = true;

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUserAdd.clsUser.strEmailAddress = clsUserAdd.clsUser.strEmailAddress.ToLower();

            //Save image
            if (clsUserAdd.strCropImageData != null && clsUserAdd.strCropImageData != "")
            {
                string data = clsUserAdd.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgProfile_" + clsUserAdd.clsUser.strFirstName + clsUserAdd.clsUser.strSurname + ".png";
                string strSmallImageName = "sml_imgProfile_" + clsUserAdd.clsUser.strFirstName + clsUserAdd.clsUser.strSurname + ".png";
                string newPath = GetUniquePath();

                string strDestPath = "images\\users_Images\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "images\\users_Images\\" + newPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }

                clsUserAdd.clsUser.strImagePath = strDestPath.Replace("\\", "/");
                clsUserAdd.clsUser.strImageName = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;
                string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Smaller Image
                Image smallImage = (Image)ResizeImage(image, 35, 35);
                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string newPath = GetUniquePath();
                string strDestPath = "images\\users_Images\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "images\\users_Images\\" + newPath;
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "images\\defaultImages";
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }

                clsUserAdd.clsUser.strImagePath = strDestPath.Replace("\\", "/");
                clsUserAdd.clsUser.strImageName = "default_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_image.png";
                string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                //Copy small and default sizes
                System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
            }

            if (clsUserAdd.strNewPassword != null && clsUserAdd.strNewPassword != "")
                clsUserAdd.clsUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserAdd.strNewPassword);
            else
                clsUserAdd.clsUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserAdd.clsUser.strEmailAddress.ToLower());

            //Add  user
            clsUsersManager.saveUser(clsUserAdd.clsUser);

            //Add successful / notification
            TempData["bIsUserAdded"] = true;

            return RedirectToAction("UsersView", "Users");
        }

        //Edit User Info
        public ActionResult UserEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("UsersView", "Users");
            }

            clsUserEdit clsUserEdit = new clsUserEdit();
            clsUsersManager clsUsersManager = new clsUsersManager();
            clsStudentsManager clsStudentsManager = new clsStudentsManager();

            clsUserEdit.clsUser = clsUsersManager.getUserById(id);
            clsUserEdit.clsUser.clsStudent = clsStudentsManager.getStudentByID((int)clsUserEdit.clsUser.iStudentID);

            clsUserEdit.iDateOfBirthDay = clsUserEdit.clsUser.clsStudent.dtDateOfBirth.Day;
            clsUserEdit.iDateOfBirthMonth = clsUserEdit.clsUser.clsStudent.dtDateOfBirth.Month;
            clsUserEdit.iDateOfBirthYear = clsUserEdit.clsUser.clsStudent.dtDateOfBirth.Year;

            clsUserEdit.strEmailAddress = clsUserEdit.clsUser.strEmailAddress;

            if (clsUserEdit.clsUser.strImagePath != null && clsUserEdit.clsUser.strImagePath != "" && clsUserEdit.clsUser.strImageName != null && clsUserEdit.clsUser.strImageName != "")
                clsUserEdit.strFullImagePath = "/" + clsUserEdit.clsUser.strImagePath + "/" + clsUserEdit.clsUser.strImageName;

            clsRoleTypesManager clsRoleTypesManager = new clsRoleTypesManager();
            clsUserEdit.lstRoleTypes = clsRoleTypesManager.getAllRoleTypesList();

            return View(clsUserEdit);
        }

        //Edit User Info
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserEdit(clsUserEdit clsUserEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsStudentsManager clsStudentsManager = new clsStudentsManager();

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsers clsUser = clsUsersManager.getUserById(clsUserEdit.clsUser.iUserID);

            //Add student data
            clsStudents clsStudent = clsStudentsManager.getStudentByID((int)clsUser.iStudentID);
            DateTime dtDateOfBirth = new DateTime((int)clsUserEdit.iDateOfBirthYear, (int)clsUserEdit.iDateOfBirthMonth, (int)clsUserEdit.iDateOfBirthDay);

            clsStudent.dtDateOfBirth = dtDateOfBirth;
            clsStudent.bIsPaidSubscription = clsUserEdit.clsUser.clsStudent.bIsPaidSubscription;
            clsStudent.strGender = clsUserEdit.clsUser.clsStudent.strGender;
            clsStudent.strNationality = clsUserEdit.clsUser.clsStudent.strNationality;
            clsStudent.strRegion = clsUserEdit.clsUser.clsStudent.strRegion;
            clsStudent.bHigherEducation = clsUserEdit.clsUser.clsStudent.bHigherEducation;
            clsStudent.strInstitutionName = clsUserEdit.clsUser.clsStudent.strInstitutionName;
            clsStudent.strCourse = clsUserEdit.clsUser.clsStudent.strCourse;
            clsStudent.strModules = clsUserEdit.clsUser.clsStudent.strModules;

            clsStudentsManager.saveStudent(clsStudent);
            clsUser.bIsStudent = true;

            //Set Role type and password
            if (clsUserEdit.bResetPassword == true)
            {
                if (clsUserEdit.strNewPassword != null && clsUserEdit.strNewPassword != "")
                    clsUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUserEdit.strNewPassword);
                else
                    clsUser.strPassword = clsCommonFunctions.GetMd5Sum(clsUser.strEmailAddress.ToLower());
            }
            clsUser.iRoleTypeID = clsUserEdit.clsUser.iRoleTypeID;

            clsUser.strFirstName = clsUserEdit.clsUser.strFirstName;
            clsUser.strSurname = clsUserEdit.clsUser.strSurname;
            clsUser.strContactNumber = clsUserEdit.clsUser.strContactNumber;
            clsUser.strBiographicalInfo = clsUserEdit.clsUser.strBiographicalInfo;

            //Save image
            if (clsUserEdit.strCropImageData != null && clsUserEdit.strCropImageData != "")
            {
                string data = clsUserEdit.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgProfile_" + clsUserEdit.clsUser.strFirstName + clsUserEdit.clsUser.strSurname + ".png";
                string strSmallImageName = "sml_imgProfile_" + clsUserEdit.clsUser.strFirstName + clsUserEdit.clsUser.strSurname + ".png";

                string strDestPath = "";
                if (clsUser.strImagePath != null && clsUser.strImagePath != "")
                {
                    strDestPath = clsUser.strImagePath.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniquePath();
                    strDestPath = "images\\users_Images\\" + newPath;
                    clsUser.strImagePath = strDestPath;
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsUser.strImageName = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;
                string strSmallImagePath = strFullDestPath + "\\" + strSmallImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Smaller Image
                Image smallImage = (Image)ResizeImage(image, 35, 35);
                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
                smallImage.Save(strSmallImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "images\\defaultImages";

                string strDestPath = "";
                if (clsUser.strImagePath != null && clsUser.strImagePath != "")
                {
                    strDestPath = clsUser.strImagePath.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniquePath();
                    strDestPath = "images\\users_Images\\" + newPath;
                    clsUser.strImagePath = strDestPath;
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsUser.strImageName = "default_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_image.png";
                string strSmallImagePath = strFullDestPath + "\\" + "sml_default_image.png";

                //Copy small and default sizes
                System.IO.File.Copy(strDefaultImagespath + "\\default_image.png", strImagePath);
                System.IO.File.Copy(strDefaultImagespath + "\\sml_default_image.png", strSmallImagePath);
            }

            //Update  user
            clsUsersManager.saveUser(clsUser);

            //Add successful / notification
            TempData["bIsUserUpdated"] = true;

            return RedirectToAction("UsersView", "Users");
        }

        //Delete User
        [HttpPost]
        public ActionResult UserDelete(int iUserID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iUserID == 0)
            {
                return RedirectToAction("UsersView", "Users");
            }

            clsStudentsManager clsStudentsManager = new clsStudentsManager();
            clsUsersManager clsUsersManager = new clsUsersManager();

            clsUsers clsUser = clsUsersManager.getUserById(iUserID);
            clsStudentsManager.removeStudentByID(clsUser.iStudentID);
            clsUsersManager.removeUserByID(iUserID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsUserDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if email already exists
        [HttpPost]
        public JsonResult checkIfUserExists([Bind(Prefix = "clsUser.strEmailAddress")] string strEmailAddress)
        {
            bool bCanUseEmail = false;
            clsUsersManager clsUsersManager = new clsUsersManager();
            if (strEmailAddress != null && strEmailAddress != "")
                strEmailAddress = strEmailAddress.ToLower();
            bool bDoesUserEmailExist = clsUsersManager.checkIfUserExists(strEmailAddress);
            if (bDoesUserEmailExist == false)
                bCanUseEmail = true;
            return Json(bCanUseEmail, JsonRequestBehavior.AllowGet);
        }

        #region IMAGE FUNCTIONS
        //Get unique user path
        private string GetUniquePath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "images\\users_Images";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\ProfileImage" + iCount) == true)
            {
                iCount++;
            }
            return "ProfileImage" + iCount;
        }
        //Resize image
        private Bitmap ResizeImage(Image image, int width, int height)
        {
            Rectangle recRectangle = new Rectangle(0, 0, width, height);
            Bitmap bitImage = new Bitmap(width, height);

            bitImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(bitImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapImageMode = new ImageAttributes())
                {
                    wrapImageMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, recRectangle, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapImageMode);
                }
            }

            return bitImage;
        }
        #endregion
    }
}