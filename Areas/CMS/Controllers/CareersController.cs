﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.Careers;
using CareerWizCMS.Model_Manager;
using CareerWiz;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace CareerWizCMS.Controllers
{
    public class CareersController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: Careers
        public ActionResult CareersView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCareersView clsCareersView = new clsCareersView();
            clsCareersManager clsCareersManager = new clsCareersManager();
            clsCareersView.lstCareers = clsCareersManager.getAllCareersList();

            return View(clsCareersView);
        }

        public ActionResult CareerAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCareers clsCareer = new clsCareers();

            return View(clsCareer);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CareerAdd(clsCareers clsCareer)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCareersManager clsCareersManager = new clsCareersManager();
            clsCareersManager.saveCareer(clsCareer);

            //Add successful / notification
            TempData["bIsCareerAdded"] = true;

            return RedirectToAction("CareersView", "Careers");
        }

        //Edit CMS Page
        public ActionResult CareerEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("CareersView", "Careers");
            }
            clsCareerEdit clsCareerEdit = new clsCareerEdit();
            clsCareersManager clsCareersManager = new clsCareersManager();

            clsCareerEdit.clsCareer = clsCareersManager.getCareerByID(id);
            clsCareerEdit.strTitle = clsCareerEdit.clsCareer.strTitle;

            return View(clsCareerEdit);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CareerEdit(clsCareerEdit clsCareerEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCareersManager clsCareersManager = new clsCareersManager();
            clsCareers clsExistingCareer = clsCareersManager.getCareerByID(clsCareerEdit.clsCareer.iCareerID);

            //clsExistingCareer.strTitle = clsCareerEdit.strTitle;
            clsExistingCareer.strDescription = clsCareerEdit.clsCareer.strDescription;
            clsExistingCareer.strExpectations = clsCareerEdit.clsCareer.strExpectations;
            clsExistingCareer.strRequiredSkills = clsCareerEdit.clsCareer.strRequiredSkills;
            clsExistingCareer.strAverageSalary = clsCareerEdit.clsCareer.strAverageSalary;

            clsCareersManager.saveCareer(clsExistingCareer);

            //Add successful / notification
            TempData["bIsCareerUpdated"] = true;

            return RedirectToAction("CareersView", "Careers");
        }

        //Delete Page
        [HttpPost]
        public ActionResult CareerDelete(int iCareerID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iCareerID == 0)
            {
                return RedirectToAction("CareersView", "Careers");
            }

            clsCareersManager clsCareersManager = new clsCareersManager();
            clsCareersManager.removeCareerByID(iCareerID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsCareerDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfCareerExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblCareers.Any(Career => Career.strTitle.ToLower() == strTitle.ToLower() && Career.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult CareersImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CareersImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("CareersImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsCareersManager clsCareersManager = new clsCareersManager();

            List<clsCareers> lstCareers = clsCareersManager.getAllCareersList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Careers Table
                        clsCareers clsCareer = new clsCareers();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exCareerTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Career Title
                                if (exCareerTitle == null || exCareerTitle == "")
                                    continue;
                                if (lstCareers.Any(Career => Career.strTitle.ToLower() == exCareerTitle.ToLower()))
                                    continue;

                                clsCareer.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Career Title
                                clsCareer.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Career Description
                                clsCareer.strExpectations = excelSheet.GetRow(row).GetCell(2).ToString(); //Career Expectations
                                clsCareer.strRequiredSkills = excelSheet.GetRow(row).GetCell(3).ToString(); //Career Required Skills
                                clsCareer.strAverageSalary = excelSheet.GetRow(row).GetCell(4).ToString(); //Career Average Salary

                                //lstCareers.Add(clsCareer);
                                clsCareersManager.saveCareer(clsCareer);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("CareersImportError", "Could not import Careers. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsCareersImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Careers Table
                        clsCareers clsCareer = new clsCareers();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exCareerTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Career Title
                                if (exCareerTitle == null || exCareerTitle == "")
                                    continue;
                                if (lstCareers.Any(Career => Career.strTitle.ToLower() == exCareerTitle.ToLower()))
                                    continue;

                                clsCareer.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Career Title
                                clsCareer.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Career Description
                                clsCareer.strExpectations = excelSheet.GetRow(row).GetCell(2).ToString(); //Career Expectations
                                clsCareer.strRequiredSkills = excelSheet.GetRow(row).GetCell(3).ToString(); //Career Required Skills
                                clsCareer.strAverageSalary = excelSheet.GetRow(row).GetCell(4).ToString(); //Career Average Salary

                                //lstCareers.Add(clsCareer);
                                clsCareersManager.saveCareer(clsCareer);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("CareersImportError", "Could not import Careers. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsCareersImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("CareersImportError", "Could not import Careers. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("CareersView", "Careers");
        }
    }
}