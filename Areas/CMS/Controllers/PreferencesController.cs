﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.Preferences;
using CareerWizCMS.Model_Manager;
using CareerWiz;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace CareerWizCMS.Controllers
{
    public class PreferencesController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: Preferences
        public ActionResult PreferencesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPreferencesView clsPreferencesView = new clsPreferencesView();
            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            clsPreferencesView.lstPreferences = clsPreferencesManager.getAllPreferencesList();

            return View(clsPreferencesView);
        }

        public ActionResult PreferenceAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPreferences clsPreference = new clsPreferences();

            return View(clsPreference);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreferenceAdd(clsPreferences clsPreference)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            clsPreferencesManager.savePreference(clsPreference);

            //Add successful / notification
            TempData["bIsPreferenceAdded"] = true;

            return RedirectToAction("PreferencesView", "Preferences");
        }

        //Edit CMS Page
        public ActionResult PreferenceEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("PreferencesView", "Preferences");
            }

            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            clsPreferences clsPreference = clsPreferencesManager.getPreferenceByID(id);

            return View(clsPreference);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreferenceEdit(clsPreferences clsPreference)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            clsPreferences clsExistingPreference = clsPreferencesManager.getPreferenceByID(clsPreference.iPreferenceID);

            clsExistingPreference.strTitle = clsPreference.strTitle;
            clsPreferencesManager.savePreference(clsExistingPreference);

            //Add successful / notification
            TempData["bIsPreferenceUpdated"] = true;

            return RedirectToAction("PreferencesView", "Preferences");
        }

        //Delete Page
        [HttpPost]
        public ActionResult PreferenceDelete(int iPreferenceID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iPreferenceID == 0)
            {
                return RedirectToAction("PreferencesView", "Preferences");
            }

            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            clsPreferencesManager.removePreferenceByID(iPreferenceID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsPreferenceDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfPreferenceExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblPreferences.Any(Preference => Preference.strTitle.ToLower() == strTitle.ToLower() && Preference.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult PreferencesImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreferencesImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("PreferencesImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();

            List<clsPreferences> lstPreferences = clsPreferencesManager.getAllPreferencesList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Preferences Table
                        clsPreferences clsPreference = new clsPreferences();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exPreferenceTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Preference Title
                                if (exPreferenceTitle == null || exPreferenceTitle == "")
                                    continue;
                                if (lstPreferences.Any(Preference => Preference.strTitle.ToLower() == exPreferenceTitle.ToLower()))
                                    continue;

                                clsPreference.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Preference Title

                                //lstPreferences.Add(clsPreference);
                                clsPreferencesManager.savePreference(clsPreference);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("PreferencesImportError", "Could not import Preferences. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsPreferencesImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Preferences Table
                        clsPreferences clsPreference = new clsPreferences();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exPreferenceTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Preference Title
                                if (exPreferenceTitle == null || exPreferenceTitle == "")
                                    continue;
                                if (lstPreferences.Any(Preference => Preference.strTitle.ToLower() == exPreferenceTitle.ToLower()))
                                    continue;

                                clsPreference.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Preference Title

                                //lstPreferences.Add(clsPreference);
                                clsPreferencesManager.savePreference(clsPreference);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("PreferencesImportError", "Could not import Preferences. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsPreferencesImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("PreferencesImportError", "Could not import Preferences. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("PreferencesView", "Preferences");
        }
    }
}