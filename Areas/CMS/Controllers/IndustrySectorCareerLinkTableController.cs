﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.IndustrySectorCareerLinkTable;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class IndustrySectorCareerLinkTableController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: IndustrySectorCareerLinkTable
        public ActionResult IndustrySectorCareerLinkTableView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorCareerLinkTableView clsIndustrySectorCareerLinkTableView = new clsIndustrySectorCareerLinkTableView();
            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
            clsIndustrySectorCareerLinkTableView.lstIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.getAllIndustrySectorCareerLinkTableList();

            return View(clsIndustrySectorCareerLinkTableView);
        }

        public ActionResult IndustrySectorCareerLinkTableAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorCareerLinkTableAdd clsIndustrySectorCareerLinkTableAdd = new clsIndustrySectorCareerLinkTableAdd();
            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsCareersManager clsCareersManager = new clsCareersManager();
            
            clsIndustrySectorCareerLinkTableAdd.clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();

            clsIndustrySectorCareerLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsIndustrySectorCareerLinkTableAdd.lstCareers = clsCareersManager.getAllCareersList();

            return View(clsIndustrySectorCareerLinkTableAdd);
        }

        //Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IndustrySectorCareerLinkTableAdd(clsIndustrySectorCareerLinkTableAdd clsIndustrySectorCareerLinkTableAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool dDoesRecordExist = db.tblIndustrySectorCareerLinkTable.Any(IndustrySectorCareerLinkTable => IndustrySectorCareerLinkTable.iIndustrySectorID == clsIndustrySectorCareerLinkTableAdd.clsIndustrySectorCareerLinkTable.iIndustrySectorID
                                    && IndustrySectorCareerLinkTable.iCareerID == clsIndustrySectorCareerLinkTableAdd.clsIndustrySectorCareerLinkTable.iCareerID
                                    && IndustrySectorCareerLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsIndustrySectorCareerLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsCareersManager clsCareersManager = new clsCareersManager();

                clsIndustrySectorCareerLinkTableAdd.clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();

                clsIndustrySectorCareerLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
                clsIndustrySectorCareerLinkTableAdd.lstCareers = clsCareersManager.getAllCareersList();

                return View(clsIndustrySectorCareerLinkTableAdd);
            }

            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
            clsIndustrySectorCareerLinkTableManager.saveIndustrySectorCareerLinkTable(clsIndustrySectorCareerLinkTableAdd.clsIndustrySectorCareerLinkTable);

            //Add successful / notification
            TempData["bIsIndustrySectorCareerLinkTableAdded"] = true;

            return RedirectToAction("IndustrySectorCareerLinkTableView", "IndustrySectorCareerLinkTable");
        }

        //Edit
        public ActionResult IndustrySectorCareerLinkTableEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("IndustrySectorCareerLinkTableView", "IndustrySectorCareerLinkTable");
            }

            clsIndustrySectorCareerLinkTableEdit clsIndustrySectorCareerLinkTableEdit = new clsIndustrySectorCareerLinkTableEdit();
            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
            clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.getIndustrySectorCareerLinkTableByID(id);

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsCareersManager clsCareersManager = new clsCareersManager();

            clsIndustrySectorCareerLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsIndustrySectorCareerLinkTableEdit.lstCareers = clsCareersManager.getAllCareersList();

            return View(clsIndustrySectorCareerLinkTableEdit);
        }

        //Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IndustrySectorCareerLinkTableEdit(clsIndustrySectorCareerLinkTableEdit clsIndustrySectorCareerLinkTableEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();

            bool dDoesRecordExist = db.tblIndustrySectorCareerLinkTable.Any(IndustrySectorCareerLinkTable => IndustrySectorCareerLinkTable.iIndustrySectorID == clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable.iIndustrySectorID
                                    && IndustrySectorCareerLinkTable.iCareerID == clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable.iCareerID
                                    && IndustrySectorCareerLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsIndustrySectorCareerLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsCareersManager clsCareersManager = new clsCareersManager();

                clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.getIndustrySectorCareerLinkTableByID(clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID);

                clsIndustrySectorCareerLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
                clsIndustrySectorCareerLinkTableEdit.lstCareers = clsCareersManager.getAllCareersList();
                return View(clsIndustrySectorCareerLinkTableEdit);
            }
            
            clsIndustrySectorCareerLinkTableManager.saveIndustrySectorCareerLinkTable(clsIndustrySectorCareerLinkTableEdit.clsIndustrySectorCareerLinkTable);

            //Add successful / notification
            TempData["bIsIndustrySectorCareerLinkTableUpdated"] = true;

            return RedirectToAction("IndustrySectorCareerLinkTableView", "IndustrySectorCareerLinkTable");
        }

        //Delete
        [HttpPost]
        public ActionResult IndustrySectorCareerLinkTableDelete(int iIndustrySectorCareerLinkTableID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iIndustrySectorCareerLinkTableID == 0)
            {
                return RedirectToAction("IndustrySectorCareerLinkTableView", "IndustrySectorCareerLinkTable");
            }

            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
            clsIndustrySectorCareerLinkTableManager.removeIndustrySectorCareerLinkTableByID(iIndustrySectorCareerLinkTableID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsIndustrySectorCareerLinkTableDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}