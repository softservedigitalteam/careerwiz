﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.Opportunities;
using CareerWizCMS.Model_Manager;
using CareerWiz;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace CareerWizCMS.Controllers
{
    public class OpportunitiesController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: Opportunities
        public ActionResult OpportunitiesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunitiesView clsOpportunitiesView = new clsOpportunitiesView();
            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();
            clsOpportunitiesView.lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();

            return View(clsOpportunitiesView);
        }

        public ActionResult OpportunityAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunities clsOpportunity = new clsOpportunities();

            return View(clsOpportunity);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OpportunityAdd(clsOpportunities clsOpportunity)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();
            clsOpportunitiesManager.saveOpportunity(clsOpportunity);

            //Add successful / notification
            TempData["bIsOpportunityAdded"] = true;

            return RedirectToAction("OpportunitiesView", "Opportunities");
        }

        //Edit CMS Page
        public ActionResult OpportunityEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("OpportunitiesView", "Opportunities");
            }

            clsOpportunityEdit clsOpportunityEdit = new clsOpportunityEdit();
            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

            clsOpportunityEdit.clsOpportunity = clsOpportunitiesManager.getOpportunityByID(id);
            clsOpportunityEdit.strTitle = clsOpportunityEdit.clsOpportunity.strTitle;

            return View(clsOpportunityEdit);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OpportunityEdit(clsOpportunityEdit clsOpportunityEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();
            clsOpportunities clsExistingOpportunity = clsOpportunitiesManager.getOpportunityByID(clsOpportunityEdit.clsOpportunity.iOpportunityID);

            //clsExistingOpportunity.strTitle = clsOpportunity.strTitle;
            clsExistingOpportunity.strDescription = clsOpportunityEdit.clsOpportunity.strDescription;

            clsOpportunitiesManager.saveOpportunity(clsExistingOpportunity);

            //Add successful / notification
            TempData["bIsOpportunityUpdated"] = true;

            return RedirectToAction("OpportunitiesView", "Opportunities");
        }

        //Delete Page
        [HttpPost]
        public ActionResult OpportunityDelete(int iOpportunityID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iOpportunityID == 0)
            {
                return RedirectToAction("OpportunitiesView", "Opportunities");
            }

            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();
            clsOpportunitiesManager.removeOpportunityByID(iOpportunityID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsOpportunityDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfOpportunityExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblOpportunities.Any(Opportunity => Opportunity.strTitle.ToLower() == strTitle.ToLower() && Opportunity.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult OpportunitiesImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OpportunitiesImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("OpportunitiesImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

            List<clsOpportunities> lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Opportunities Table
                        clsOpportunities clsOpportunity = new clsOpportunities();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exOpportunityTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Opportunity Title
                                if (exOpportunityTitle == null || exOpportunityTitle == "")
                                    continue;
                                if (lstOpportunities.Any(Opportunity => Opportunity.strTitle.ToLower() == exOpportunityTitle.ToLower()))
                                    continue;

                                clsOpportunity.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Opportunity Title
                                clsOpportunity.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Opportunity Description

                                //lstOpportunities.Add(clsOpportunity);
                                clsOpportunitiesManager.saveOpportunity(clsOpportunity);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("OpportunitiesImportError", "Could not import Opportunities. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsOpportunitiesImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Opportunities Table
                        clsOpportunities clsOpportunity = new clsOpportunities();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exOpportunityTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Opportunity Title
                                if (exOpportunityTitle == null || exOpportunityTitle == "")
                                    continue;
                                if (lstOpportunities.Any(Opportunity => Opportunity.strTitle.ToLower() == exOpportunityTitle.ToLower()))
                                    continue;

                                clsOpportunity.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Opportunity Title
                                clsOpportunity.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Opportunity Description

                                //lstOpportunities.Add(clsOpportunity);
                                clsOpportunitiesManager.saveOpportunity(clsOpportunity);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("OpportunitiesImportError", "Could not import Opportunities. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsOpportunitiesImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("OpportunitiesImportError", "Could not import Opportunities. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("OpportunitiesView", "Opportunities");
        }
    }
}