﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.OpportunityIndustrySectorLinkTable;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class OpportunityIndustrySectorLinkTableController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: OpportunityIndustrySectorLinkTable
        public ActionResult OpportunityIndustrySectorLinkTableView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunityIndustrySectorLinkTableView clsOpportunityIndustrySectorLinkTableView = new clsOpportunityIndustrySectorLinkTableView();
            clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
            clsOpportunityIndustrySectorLinkTableView.lstOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.getAllOpportunityIndustrySectorLinkTableList();

            return View(clsOpportunityIndustrySectorLinkTableView);
        }

        public ActionResult OpportunityIndustrySectorLinkTableAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunityIndustrySectorLinkTableAdd clsOpportunityIndustrySectorLinkTableAdd = new clsOpportunityIndustrySectorLinkTableAdd();
            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();
            
            clsOpportunityIndustrySectorLinkTableAdd.clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();

            clsOpportunityIndustrySectorLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsOpportunityIndustrySectorLinkTableAdd.lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();

            return View(clsOpportunityIndustrySectorLinkTableAdd);
        }

        //Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OpportunityIndustrySectorLinkTableAdd(clsOpportunityIndustrySectorLinkTableAdd clsOpportunityIndustrySectorLinkTableAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool dDoesRecordExist = db.tblOpportunityIndustrySectorLinkTable.Any(OpportunityIndustrySectorLinkTable => OpportunityIndustrySectorLinkTable.iIndustrySectorID == clsOpportunityIndustrySectorLinkTableAdd.clsOpportunityIndustrySectorLinkTable.iIndustrySectorID
                                    && OpportunityIndustrySectorLinkTable.iOpportunityID == clsOpportunityIndustrySectorLinkTableAdd.clsOpportunityIndustrySectorLinkTable.iOpportunityID
                                    && OpportunityIndustrySectorLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsOpportunityIndustrySectorLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

                clsOpportunityIndustrySectorLinkTableAdd.clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();
                clsOpportunityIndustrySectorLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();

                clsOpportunityIndustrySectorLinkTableAdd.lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();
                return View(clsOpportunityIndustrySectorLinkTableAdd);
            }

            clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
            clsOpportunityIndustrySectorLinkTableManager.saveOpportunityIndustrySectorLinkTable(clsOpportunityIndustrySectorLinkTableAdd.clsOpportunityIndustrySectorLinkTable);

            //Add successful / notification
            TempData["bIsOpportunityIndustrySectorLinkTableAdded"] = true;

            return RedirectToAction("OpportunityIndustrySectorLinkTableView", "OpportunityIndustrySectorLinkTable");
        }

        //Edit
        public ActionResult OpportunityIndustrySectorLinkTableEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("OpportunityIndustrySectorLinkTableView", "OpportunityIndustrySectorLinkTable");
            }

            clsOpportunityIndustrySectorLinkTableEdit clsOpportunityIndustrySectorLinkTableEdit = new clsOpportunityIndustrySectorLinkTableEdit();
            clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
            clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.getOpportunityIndustrySectorLinkTableByID(id);

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

            clsOpportunityIndustrySectorLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsOpportunityIndustrySectorLinkTableEdit.lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();

            return View(clsOpportunityIndustrySectorLinkTableEdit);
        }

        //Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OpportunityIndustrySectorLinkTableEdit(clsOpportunityIndustrySectorLinkTableEdit clsOpportunityIndustrySectorLinkTableEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();

            bool dDoesRecordExist = db.tblOpportunityIndustrySectorLinkTable.Any(OpportunityIndustrySectorLinkTable => OpportunityIndustrySectorLinkTable.iIndustrySectorID == clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable.iIndustrySectorID
                                    && OpportunityIndustrySectorLinkTable.iOpportunityID == clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable.iOpportunityID
                                    && OpportunityIndustrySectorLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsOpportunityIndustrySectorLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

                clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.getOpportunityIndustrySectorLinkTableByID(clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID);

                clsOpportunityIndustrySectorLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
                clsOpportunityIndustrySectorLinkTableEdit.lstOpportunities = clsOpportunitiesManager.getAllOpportunitiesList();
                return View(clsOpportunityIndustrySectorLinkTableEdit);
            }
            
            clsOpportunityIndustrySectorLinkTableManager.saveOpportunityIndustrySectorLinkTable(clsOpportunityIndustrySectorLinkTableEdit.clsOpportunityIndustrySectorLinkTable);

            //Add successful / notification
            TempData["bIsOpportunityIndustrySectorLinkTableUpdated"] = true;

            return RedirectToAction("OpportunityIndustrySectorLinkTableView", "OpportunityIndustrySectorLinkTable");
        }

        //Delete
        [HttpPost]
        public ActionResult OpportunityIndustrySectorLinkTableDelete(int iOpportunityIndustrySectorLinkTableID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iOpportunityIndustrySectorLinkTableID == 0)
            {
                return RedirectToAction("OpportunityIndustrySectorLinkTableView", "OpportunityIndustrySectorLinkTable");
            }

            clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
            clsOpportunityIndustrySectorLinkTableManager.removeOpportunityIndustrySectorLinkTableByID(iOpportunityIndustrySectorLinkTableID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsOpportunityIndustrySectorLinkTableDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}