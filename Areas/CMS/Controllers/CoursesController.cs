﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.Courses;
using CareerWizCMS.Model_Manager;
using CareerWiz;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace CareerWizCMS.Controllers
{
    public class CoursesController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: Courses
        public ActionResult CoursesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCoursesView clsCoursesView = new clsCoursesView();
            clsCoursesManager clsCoursesManager = new clsCoursesManager();
            clsCoursesView.lstCourses = clsCoursesManager.getAllCoursesList();

            return View(clsCoursesView);
        }

        public ActionResult CourseAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCourses clsCourse = new clsCourses();

            return View(clsCourse);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CourseAdd(clsCourses clsCourse)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCoursesManager clsCoursesManager = new clsCoursesManager();
            clsCoursesManager.saveCourse(clsCourse);

            //Add successful / notification
            TempData["bIsCourseAdded"] = true;

            return RedirectToAction("CoursesView", "Courses");
        }

        //Edit CMS Page
        public ActionResult CourseEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("CoursesView", "Courses");
            }
            clsCourseEdit clsCourseEdit = new clsCourseEdit();
            clsCoursesManager clsCoursesManager = new clsCoursesManager();

            clsCourseEdit.clsCourse = clsCoursesManager.getCourseByID(id);
            clsCourseEdit.strTitle = clsCourseEdit.clsCourse.strTitle;

            return View(clsCourseEdit);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CourseEdit(clsCourseEdit clsCourseEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsCoursesManager clsCoursesManager = new clsCoursesManager();
            clsCourses clsExistingCourse = clsCoursesManager.getCourseByID(clsCourseEdit.clsCourse.iCourseID);

            //clsExistingCourse.strTitle = clsCourse.strTitle;
            clsExistingCourse.strDescription = clsCourseEdit.clsCourse.strDescription;
            clsCoursesManager.saveCourse(clsExistingCourse);

            //Add successful / notification
            TempData["bIsCourseUpdated"] = true;

            return RedirectToAction("CoursesView", "Courses");
        }

        //Delete Page
        [HttpPost]
        public ActionResult CourseDelete(int iCourseID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iCourseID == 0)
            {
                return RedirectToAction("CoursesView", "Courses");
            }

            clsCoursesManager clsCoursesManager = new clsCoursesManager();
            clsCoursesManager.removeCourseByID(iCourseID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsCourseDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfCourseExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblCourses.Any(Course => Course.strTitle.ToLower() == strTitle.ToLower() && Course.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult CoursesImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CoursesImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("CoursesImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsCoursesManager clsCoursesManager = new clsCoursesManager();

            List<clsCourses> lstCourses = clsCoursesManager.getAllCoursesList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Courses Table
                        clsCourses clsCourse = new clsCourses();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exCourseTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Course Title
                                if (exCourseTitle == null || exCourseTitle == "")
                                    continue;
                                if (lstCourses.Any(Course => Course.strTitle.ToLower() == exCourseTitle.ToLower()))
                                    continue;

                                clsCourse.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Course Title
                                clsCourse.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Course Description

                                //lstCourses.Add(clsCourse);
                                clsCoursesManager.saveCourse(clsCourse);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("CoursesImportError", "Could not import Courses. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsCoursesImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Courses Table
                        clsCourses clsCourse = new clsCourses();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exCourseTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Course Title
                                if (exCourseTitle == null || exCourseTitle == "")
                                    continue;
                                if (lstCourses.Any(Course => Course.strTitle.ToLower() == exCourseTitle.ToLower()))
                                    continue;

                                clsCourse.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Course Title
                                clsCourse.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Course Description

                                //lstCourses.Add(clsCourse);
                                clsCoursesManager.saveCourse(clsCourse);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("CoursesImportError", "Could not import Courses. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsCoursesImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("CoursesImportError", "Could not import Courses. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("CoursesView", "Courses");
        }
    }
}