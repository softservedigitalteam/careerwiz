﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.Institutions;
using CareerWizCMS.Model_Manager;
using CareerWizCMS.View_Models;
using CareerWizCMS.View_Models.Users;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CareerWizCMS.Assistant_Classes;
using CareerWiz;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace CareerWizCMS.Controllers
{
    public class InstitutionsController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: Institutions
        public ActionResult InstitutionsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionsView clsInstitutionsView = new clsInstitutionsView();
            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsInstitutionsView.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();

            return View(clsInstitutionsView);
        }

        public ActionResult InstitutionAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionsAdd clsInstitutionsAdd = new clsInstitutionsAdd();
            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();

            clsInstitutionsAdd.lstInstitutionTypes = clsInstitutionTypesManager.getAllInstitutionTypesList();
            return View(clsInstitutionsAdd);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionAdd(clsInstitutionsAdd clsInstitutionsAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

            //Save image
            if (clsInstitutionsAdd.strCropImageData != null && clsInstitutionsAdd.strCropImageData != "")
            {
                string data = clsInstitutionsAdd.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgInstitution_" + clsInstitutionsAdd.clsInstitution.strTitle + ".png";
                string newPath = GetUniquePath();

                string strDestPath = "images\\institution_Images\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "images\\institution_Images\\" + newPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }

                clsInstitutionsAdd.clsInstitution.strImagePath = strDestPath.Replace("\\", "/");
                clsInstitutionsAdd.clsInstitution.strImageName = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string newPath = GetUniquePath();
                string strDestPath = "images\\institution_Images\\" + newPath;
                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + "images\\institution_Images\\" + newPath;
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "images\\defaultImages";
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }

                clsInstitutionsAdd.clsInstitution.strImagePath = strDestPath.Replace("\\", "/");
                clsInstitutionsAdd.clsInstitution.strImageName = "default_institution_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_institution_image.png";

                //Copy default size
                System.IO.File.Copy(strDefaultImagespath + "\\default_institution_image.png", strImagePath);
            }

            clsInstitutionsManager.saveInstitution(clsInstitutionsAdd.clsInstitution);

            //Add successful / notification
            TempData["bIsInstitutionAdded"] = true;

            return RedirectToAction("InstitutionsView", "Institutions");
        }

        //Edit CMS Page
        public ActionResult InstitutionEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("InstitutionsView", "Institutions");
            }
            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionEdit clsInstitutionEdit = new clsInstitutionEdit();
            
            clsInstitutionEdit.clsInstitution = clsInstitutionsManager.getInstitutionByID(id);
            clsInstitutionEdit.strTitle = clsInstitutionEdit.clsInstitution.strTitle;

            if (clsInstitutionEdit.clsInstitution.strImagePath != null && clsInstitutionEdit.clsInstitution.strImagePath != "" && clsInstitutionEdit.clsInstitution.strImageName != null && clsInstitutionEdit.clsInstitution.strImageName != "")
                clsInstitutionEdit.strFullImagePath = "/" + clsInstitutionEdit.clsInstitution.strImagePath + "/" + clsInstitutionEdit.clsInstitution.strImageName;

            clsInstitutionEdit.lstInstitutionTypes = clsInstitutionTypesManager.getAllInstitutionTypesList();


            return View(clsInstitutionEdit);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionEdit(clsInstitutionEdit clsInstitutionEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsInstitutions clsExistingInstitution = clsInstitutionsManager.getInstitutionByID(clsInstitutionEdit.clsInstitution.iInstitutionID);

            //clsExistingInstitution.strTitle = clsInstitutionEdit.clsInstitution.strTitle;
            clsExistingInstitution.strDescriptionOverview = clsInstitutionEdit.clsInstitution.strDescriptionOverview;
            clsExistingInstitution.strDescription = clsInstitutionEdit.clsInstitution.strDescription;
            clsExistingInstitution.iInstitutionTypeID = clsInstitutionEdit.clsInstitution.iInstitutionTypeID;
            clsExistingInstitution.strContactDetails = clsInstitutionEdit.clsInstitution.strContactDetails;

            clsExistingInstitution.strVisionMission = clsInstitutionEdit.clsInstitution.strVisionMission;
            clsExistingInstitution.strCampusLife = clsInstitutionEdit.clsInstitution.strCampusLife;
            clsExistingInstitution.strImportantDatesInformation = clsInstitutionEdit.clsInstitution.strImportantDatesInformation;
            clsExistingInstitution.strInstitutionURL = clsInstitutionEdit.clsInstitution.strInstitutionURL;

            //Save image
            if (clsInstitutionEdit.strCropImageData != null && clsInstitutionEdit.strCropImageData != "")
            {
                string data = clsInstitutionEdit.strCropImageData;
                data = data.Replace("data:image/png;base64,", "");
                string strImageName = "imgInstitution_" + clsExistingInstitution.strTitle + ".png";

                string strDestPath = "";
                if (clsExistingInstitution.strImagePath != null && clsExistingInstitution.strImagePath != "")
                {
                    strDestPath = clsExistingInstitution.strImagePath.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniquePath();
                    strDestPath = "images\\institution_Images\\" + newPath;
                    clsExistingInstitution.strImagePath = strDestPath;
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsExistingInstitution.strImageName = strImageName;

                string strImagePath = strFullDestPath + "\\" + strImageName;

                byte[] bytes = Convert.FromBase64String(data);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                //Save images
                image.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Png);
            }
            else //save default images
            {
                string strDefaultImagespath = AppDomain.CurrentDomain.BaseDirectory + "images\\defaultImages";

                string strDestPath = "";
                if (clsExistingInstitution.strImagePath != null && clsExistingInstitution.strImagePath != "")
                {
                    strDestPath = clsExistingInstitution.strImagePath.Replace("/", "\\");
                }
                else
                {
                    string newPath = GetUniquePath();
                    strDestPath = "images\\institution_Images\\" + newPath;
                    clsExistingInstitution.strImagePath = strDestPath;
                }

                string strFullDestPath = AppDomain.CurrentDomain.BaseDirectory + strDestPath;
                if (!System.IO.Directory.Exists(strFullDestPath))
                {
                    System.IO.Directory.CreateDirectory(strFullDestPath);
                }
                else
                {
                    //Clear files
                    string[] strAllFilePaths = Directory.GetFiles(strFullDestPath);
                    foreach (string strFilePath in strAllFilePaths)
                        System.IO.File.Delete(strFilePath);
                }

                clsExistingInstitution.strImageName = "default_institution_image.png";

                string strImagePath = strFullDestPath + "\\" + "default_institution_image.png";

                //Copy default size
                System.IO.File.Copy(strDefaultImagespath + "\\default_institution_image.png", strImagePath);
            }

            clsInstitutionsManager.saveInstitution(clsExistingInstitution);

            //Add successful / notification
            TempData["bIsInstitutionUpdated"] = true;

            return RedirectToAction("InstitutionsView", "Institutions");
        }

        //Delete Page
        [HttpPost]
        public ActionResult InstitutionDelete(int iInstitutionID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iInstitutionID == 0)
            {
                return RedirectToAction("InstitutionsView", "Institutions");
            }

            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsInstitutionsManager.removeInstitutionByID(iInstitutionID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsInstitutionDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfInstitutionExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblInstitutions.Any(Institution => Institution.strTitle.ToLower() == strTitle.ToLower() && Institution.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }

        //Import
        public ActionResult InstitutionsImport()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            return View();
        }

        //Import
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionsImport(HttpPostedFileBase fuFileUpload)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            //Clear error message if exists
            ModelState.AddModelError("InstitutionsImportError", "");

            //Save path to local document directory to be read
            string strPathToImportedExcelDocument = AppDomain.CurrentDomain.BaseDirectory + @"Documents\" + fuFileUpload.FileName;
            fuFileUpload.SaveAs(strPathToImportedExcelDocument);

            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();

            List<clsInstitutions> lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
            List<clsInstitutionTypes> lstInstitutionTypes = clsInstitutionTypesManager.getAllInstitutionTypesList();

            //Check document extension
            string strFileExtension = Path.GetExtension(fuFileUpload.FileName);
            //Lagacy Excel Documents
            if (strFileExtension.Equals(".xls"))
            {
                //Get info from excel document and save data
                try
                {
                    HSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new HSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Institutions Table
                        clsInstitutions clsInstitution = new clsInstitutions();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exInstitutionTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Institution Title
                                if (exInstitutionTitle == null || exInstitutionTitle == "")
                                    continue;
                                if (lstInstitutions.Any(Institution => Institution.strTitle.ToLower() == exInstitutionTitle.ToLower()))
                                    continue;

                                clsInstitution.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Institution Title
                                clsInstitution.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Institution Description
                                clsInstitution.strDescriptionOverview = excelSheet.GetRow(row).GetCell(2).ToString(); //Institution Overview
                                clsInstitution.iInstitutionTypeID = Int32.Parse(excelSheet.GetRow(row).GetCell(3).ToString()); //Institution Type
                                clsInstitution.strContactDetails = excelSheet.GetRow(row).GetCell(4).ToString(); //Institution Contact Details
                                clsInstitution.strVisionMission = excelSheet.GetRow(row).GetCell(5).ToString(); //Institution Vision / Mission
                                clsInstitution.strCampusLife = excelSheet.GetRow(row).GetCell(6).ToString(); //Institution Campus Life
                                clsInstitution.strImportantDatesInformation = excelSheet.GetRow(row).GetCell(7).ToString(); //Institution Important Dates
                                clsInstitution.strInstitutionURL = excelSheet.GetRow(row).GetCell(8).ToString(); //Institution URL
                                clsInstitution.strImagePath = "";
                                clsInstitution.strImageName = "";

                                //lstInstitutions.Add(clsInstitution);
                                clsInstitutionsManager.saveInstitution(clsInstitution);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("InstitutionsImportError", "Could not import Institutions. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsInstitutionsImported"] = true;
            }
            else if (strFileExtension.Equals(".xlsx"))
            {
                //Get info from excel document and save data
                try
                {
                    XSSFWorkbook hwbExcelDocument = null;

                    using (FileStream fImportedDocument = new FileStream(strPathToImportedExcelDocument, FileMode.Open, FileAccess.Read))
                    {
                        hwbExcelDocument = new XSSFWorkbook(fImportedDocument);
                    }

                    var excelSheet = hwbExcelDocument.GetSheetAt(0);
                    //Begin after headers
                    for (int row = 1; row <= excelSheet.LastRowNum; row++)
                    {
                        //Read each row and save to Institutions Table
                        clsInstitutions clsInstitution = new clsInstitutions();

                        if (excelSheet.GetRow(row) != null) //null is when the row only contains empty cells 
                        {
                            if (excelSheet.GetRow(row).GetCell(0) != null)
                            {
                                string exInstitutionTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Institution Title
                                if (exInstitutionTitle == null || exInstitutionTitle == "")
                                    continue;
                                if (lstInstitutions.Any(Institution => Institution.strTitle.ToLower() == exInstitutionTitle.ToLower()))
                                    continue;

                                clsInstitution.strTitle = excelSheet.GetRow(row).GetCell(0).ToString(); //Institution Title
                                clsInstitution.strDescription = excelSheet.GetRow(row).GetCell(1).ToString(); //Institution Description
                                clsInstitution.strDescriptionOverview = excelSheet.GetRow(row).GetCell(2).ToString(); //Institution Overview
                                clsInstitution.iInstitutionTypeID = Int32.Parse(excelSheet.GetRow(row).GetCell(3).ToString()); //Institution Type
                                clsInstitution.strContactDetails = excelSheet.GetRow(row).GetCell(4).ToString(); //Institution Contact Details
                                clsInstitution.strVisionMission = excelSheet.GetRow(row).GetCell(5).ToString(); //Institution Vision / Mission
                                clsInstitution.strCampusLife = excelSheet.GetRow(row).GetCell(6).ToString(); //Institution Campus Life
                                clsInstitution.strImportantDatesInformation = excelSheet.GetRow(row).GetCell(7).ToString(); //Institution Important Dates
                                clsInstitution.strInstitutionURL = excelSheet.GetRow(row).GetCell(8).ToString(); //Institution URL
                                clsInstitution.strImagePath = "";
                                clsInstitution.strImageName = "";

                                //lstInstitutions.Add(clsInstitution);
                                clsInstitutionsManager.saveInstitution(clsInstitution);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strErrorMessage = ex.ToString();
                    //Delete File
                    if (System.IO.File.Exists(strPathToImportedExcelDocument))
                        System.IO.File.Delete(strPathToImportedExcelDocument);

                    ModelState.AddModelError("InstitutionsImportError", "Could not import Institutions. Please ensure the excel document is in the correct format.");
                    return View();
                }

                //Import successful / notification
                TempData["bIsInstitutionsImported"] = true;
            }
            else
            {
                //Delete File
                if (System.IO.File.Exists(strPathToImportedExcelDocument))
                    System.IO.File.Delete(strPathToImportedExcelDocument);

                ModelState.AddModelError("InstitutionsImportError", "Could not import Institutions. Please ensure the excel document is in the correct format.");
                return View();
            }

            //Delete File
            if (System.IO.File.Exists(strPathToImportedExcelDocument))
                System.IO.File.Delete(strPathToImportedExcelDocument);

            return RedirectToAction("InstitutionsView", "Institutions");
        }

        #region IMAGE FUNCTIONS
        //Get unique path
        private string GetUniquePath()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "images\\institution_Images";
            int iCount = 1;
            //### First we need to get the path
            while (System.IO.Directory.Exists(path + "\\InstitutionImage" + iCount) == true)
            {
                iCount++;
            }
            return "InstitutionImage" + iCount;
        }
        #endregion
    }
}