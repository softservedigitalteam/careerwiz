﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.InstitutionIndustrySectorLinkTable;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class InstitutionIndustrySectorLinkTableController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: InstitutionIndustrySectorLinkTable
        public ActionResult InstitutionIndustrySectorLinkTableView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionIndustrySectorLinkTableView clsInstitutionIndustrySectorLinkTableView = new clsInstitutionIndustrySectorLinkTableView();
            clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
            clsInstitutionIndustrySectorLinkTableView.lstInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.getAllInstitutionIndustrySectorLinkTableList();

            return View(clsInstitutionIndustrySectorLinkTableView);
        }

        public ActionResult InstitutionIndustrySectorLinkTableAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionIndustrySectorLinkTableAdd clsInstitutionIndustrySectorLinkTableAdd = new clsInstitutionIndustrySectorLinkTableAdd();
            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            
            clsInstitutionIndustrySectorLinkTableAdd.clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();

            clsInstitutionIndustrySectorLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsInstitutionIndustrySectorLinkTableAdd.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();

            return View(clsInstitutionIndustrySectorLinkTableAdd);
        }

        //Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionIndustrySectorLinkTableAdd(clsInstitutionIndustrySectorLinkTableAdd clsInstitutionIndustrySectorLinkTableAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool dDoesRecordExist = db.tblInstitutionIndustrySectorLinkTable.Any(InstitutionIndustrySectorLinkTable => InstitutionIndustrySectorLinkTable.iIndustrySectorID == clsInstitutionIndustrySectorLinkTableAdd.clsInstitutionIndustrySectorLinkTable.iIndustrySectorID
                                    && InstitutionIndustrySectorLinkTable.iInstitutionID == clsInstitutionIndustrySectorLinkTableAdd.clsInstitutionIndustrySectorLinkTable.iInstitutionID
                                    && InstitutionIndustrySectorLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsInstitutionIndustrySectorLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

                clsInstitutionIndustrySectorLinkTableAdd.clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();
                clsInstitutionIndustrySectorLinkTableAdd.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();

                clsInstitutionIndustrySectorLinkTableAdd.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
                return View(clsInstitutionIndustrySectorLinkTableAdd);
            }

            clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
            clsInstitutionIndustrySectorLinkTableManager.saveInstitutionIndustrySectorLinkTable(clsInstitutionIndustrySectorLinkTableAdd.clsInstitutionIndustrySectorLinkTable);

            //Add successful / notification
            TempData["bIsInstitutionIndustrySectorLinkTableAdded"] = true;

            return RedirectToAction("InstitutionIndustrySectorLinkTableView", "InstitutionIndustrySectorLinkTable");
        }

        //Edit
        public ActionResult InstitutionIndustrySectorLinkTableEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("InstitutionIndustrySectorLinkTableView", "InstitutionIndustrySectorLinkTable");
            }

            clsInstitutionIndustrySectorLinkTableEdit clsInstitutionIndustrySectorLinkTableEdit = new clsInstitutionIndustrySectorLinkTableEdit();
            clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
            clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.getInstitutionIndustrySectorLinkTableByID(id);

            clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

            clsInstitutionIndustrySectorLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
            clsInstitutionIndustrySectorLinkTableEdit.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();

            return View(clsInstitutionIndustrySectorLinkTableEdit);
        }

        //Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionIndustrySectorLinkTableEdit(clsInstitutionIndustrySectorLinkTableEdit clsInstitutionIndustrySectorLinkTableEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();

            bool dDoesRecordExist = db.tblInstitutionIndustrySectorLinkTable.Any(InstitutionIndustrySectorLinkTable => InstitutionIndustrySectorLinkTable.iIndustrySectorID == clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable.iIndustrySectorID
                                    && InstitutionIndustrySectorLinkTable.iInstitutionID == clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable.iInstitutionID
                                    && InstitutionIndustrySectorLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsInstitutionIndustrySectorLinkTableRecordExists"] = true;
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

                clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.getInstitutionIndustrySectorLinkTableByID(clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID);

                clsInstitutionIndustrySectorLinkTableEdit.lstIndustrySectors = clsIndustrySectorsManager.getAllIndustrySectorsList();
                clsInstitutionIndustrySectorLinkTableEdit.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
                return View(clsInstitutionIndustrySectorLinkTableEdit);
            }
            
            clsInstitutionIndustrySectorLinkTableManager.saveInstitutionIndustrySectorLinkTable(clsInstitutionIndustrySectorLinkTableEdit.clsInstitutionIndustrySectorLinkTable);

            //Add successful / notification
            TempData["bIsInstitutionIndustrySectorLinkTableUpdated"] = true;

            return RedirectToAction("InstitutionIndustrySectorLinkTableView", "InstitutionIndustrySectorLinkTable");
        }

        //Delete
        [HttpPost]
        public ActionResult InstitutionIndustrySectorLinkTableDelete(int iInstitutionIndustrySectorLinkTableID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iInstitutionIndustrySectorLinkTableID == 0)
            {
                return RedirectToAction("InstitutionIndustrySectorLinkTableView", "InstitutionIndustrySectorLinkTable");
            }

            clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
            clsInstitutionIndustrySectorLinkTableManager.removeInstitutionIndustrySectorLinkTableByID(iInstitutionIndustrySectorLinkTableID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsInstitutionIndustrySectorLinkTableDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}