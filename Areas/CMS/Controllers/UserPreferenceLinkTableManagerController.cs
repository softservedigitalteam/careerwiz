﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.UserPreferenceLinkTable;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class UserPreferenceLinkTableController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: UserPreferenceLinkTable
        public ActionResult UserPreferenceLinkTableView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsUserPreferenceLinkTableView clsUserPreferenceLinkTableView = new clsUserPreferenceLinkTableView();
            clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();
            clsUserPreferenceLinkTableView.lstUserPreferenceLinkTable = clsUserPreferenceLinkTableManager.getAllUserPreferenceLinkTableList();

            return View(clsUserPreferenceLinkTableView);
        }

        public ActionResult UserPreferenceLinkTableAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsUserPreferenceLinkTableAdd clsUserPreferenceLinkTableAdd = new clsUserPreferenceLinkTableAdd();
            clsUsersManager clsUsersManager = new clsUsersManager();
            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
            
            clsUserPreferenceLinkTableAdd.clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();

            clsUserPreferenceLinkTableAdd.lstUsers = clsUsersManager.getAllUsersList();
            clsUserPreferenceLinkTableAdd.lstPreferences = clsPreferencesManager.getAllPreferencesList();

            //Get full names
            if (clsUserPreferenceLinkTableAdd.lstUsers.Count > 0)
                foreach (var item in clsUserPreferenceLinkTableAdd.lstUsers)
                    item.strFirstName = item.strFirstName + " " + item.strSurname;

            return View(clsUserPreferenceLinkTableAdd);
        }

        //Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserPreferenceLinkTableAdd(clsUserPreferenceLinkTableAdd clsUserPreferenceLinkTableAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool dDoesRecordExist = db.tblUserPreferenceLinkTable.Any(UserPreferenceLinkTable => UserPreferenceLinkTable.iUserID == clsUserPreferenceLinkTableAdd.clsUserPreferenceLinkTable.iUserID
                                    && UserPreferenceLinkTable.iPreferenceID == clsUserPreferenceLinkTableAdd.clsUserPreferenceLinkTable.iPreferenceID
                                    && UserPreferenceLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsUserPreferenceLinkTableRecordExists"] = true;
                clsUsersManager clsUsersManager = new clsUsersManager();
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();

                clsUserPreferenceLinkTableAdd.clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();
                clsUserPreferenceLinkTableAdd.lstUsers = clsUsersManager.getAllUsersList();

                //Get full names
                if (clsUserPreferenceLinkTableAdd.lstUsers.Count > 0)
                    foreach (var item in clsUserPreferenceLinkTableAdd.lstUsers)
                        item.strFirstName = item.strFirstName + " " + item.strSurname;

                clsUserPreferenceLinkTableAdd.lstPreferences = clsPreferencesManager.getAllPreferencesList();
                return View(clsUserPreferenceLinkTableAdd);
            }

            clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();
            clsUserPreferenceLinkTableManager.saveUserPreferenceLinkTable(clsUserPreferenceLinkTableAdd.clsUserPreferenceLinkTable);

            //Add successful / notification
            TempData["bIsUserPreferenceLinkTableAdded"] = true;

            return RedirectToAction("UserPreferenceLinkTableView", "UserPreferenceLinkTable");
        }

        //Edit
        public ActionResult UserPreferenceLinkTableEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("UserPreferenceLinkTableView", "UserPreferenceLinkTable");
            }

            clsUserPreferenceLinkTableEdit clsUserPreferenceLinkTableEdit = new clsUserPreferenceLinkTableEdit();
            clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();
            clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable = clsUserPreferenceLinkTableManager.getUserPreferenceLinkTableByID(id);

            clsUsersManager clsUsersManager = new clsUsersManager();
            clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();

            clsUserPreferenceLinkTableEdit.lstUsers = clsUsersManager.getAllUsersList();
            clsUserPreferenceLinkTableEdit.lstPreferences = clsPreferencesManager.getAllPreferencesList();

            //Get full names
            if (clsUserPreferenceLinkTableEdit.lstUsers.Count > 0)
                foreach (var item in clsUserPreferenceLinkTableEdit.lstUsers)
                    item.strFirstName = item.strFirstName + " " + item.strSurname;

            return View(clsUserPreferenceLinkTableEdit);
        }

        //Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserPreferenceLinkTableEdit(clsUserPreferenceLinkTableEdit clsUserPreferenceLinkTableEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();

            bool dDoesRecordExist = db.tblUserPreferenceLinkTable.Any(UserPreferenceLinkTable => UserPreferenceLinkTable.iUserID == clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable.iUserID
                                    && UserPreferenceLinkTable.iPreferenceID == clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable.iPreferenceID
                                    && UserPreferenceLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsUserPreferenceLinkTableRecordExists"] = true;
                clsUsersManager clsUsersManager = new clsUsersManager();
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();

                clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable = clsUserPreferenceLinkTableManager.getUserPreferenceLinkTableByID(clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable.iUserPreferenceLinkTableID);

                clsUserPreferenceLinkTableEdit.lstUsers = clsUsersManager.getAllUsersList();
                clsUserPreferenceLinkTableEdit.lstPreferences = clsPreferencesManager.getAllPreferencesList();

                //Get full names
                if (clsUserPreferenceLinkTableEdit.lstUsers.Count > 0)
                    foreach (var item in clsUserPreferenceLinkTableEdit.lstUsers)
                        item.strFirstName = item.strFirstName + " " + item.strSurname;

                return View(clsUserPreferenceLinkTableEdit);
            }
            
            clsUserPreferenceLinkTableManager.saveUserPreferenceLinkTable(clsUserPreferenceLinkTableEdit.clsUserPreferenceLinkTable);

            //Add successful / notification
            TempData["bIsUserPreferenceLinkTableUpdated"] = true;

            return RedirectToAction("UserPreferenceLinkTableView", "UserPreferenceLinkTable");
        }

        //Delete
        [HttpPost]
        public ActionResult UserPreferenceLinkTableDelete(int iUserPreferenceLinkTableID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iUserPreferenceLinkTableID == 0)
            {
                return RedirectToAction("UserPreferenceLinkTableView", "UserPreferenceLinkTable");
            }

            clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();
            clsUserPreferenceLinkTableManager.removeUserPreferenceLinkTableByID(iUserPreferenceLinkTableID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsUserPreferenceLinkTableDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}