﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.InstitutionTypes;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class InstitutionTypesController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: InstitutionTypes
        public ActionResult InstitutionTypesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionTypesView clsInstitutionTypesView = new clsInstitutionTypesView();
            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionTypesView.lstInstitutionTypes = clsInstitutionTypesManager.getAllInstitutionTypesList();

            return View(clsInstitutionTypesView);
        }

        public ActionResult InstitutionTypeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionTypes clsInstitutionType = new clsInstitutionTypes();

            return View(clsInstitutionType);
        }

        //Add CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionTypeAdd(clsInstitutionTypes clsInstitutionType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionTypesManager.saveInstitutionType(clsInstitutionType);

            //Add successful / notification
            TempData["bIsInstitutionTypeAdded"] = true;

            return RedirectToAction("InstitutionTypesView", "InstitutionTypes");
        }

        //Edit CMS Page
        public ActionResult InstitutionTypeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("InstitutionTypesView", "InstitutionTypes");
            }

            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionTypes clsInstitutionType = clsInstitutionTypesManager.getInstitutionTypeByID(id);

            return View(clsInstitutionType);
        }

        //Edit CMS Page
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionTypeEdit(clsInstitutionTypes clsInstitutionType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionTypes clsExistingInstitutionType = clsInstitutionTypesManager.getInstitutionTypeByID(clsInstitutionType.iInstitutionTypeID);

            clsExistingInstitutionType.strTitle = clsInstitutionType.strTitle;
            clsInstitutionTypesManager.saveInstitutionType(clsExistingInstitutionType);

            //Add successful / notification
            TempData["bIsInstitutionTypeUpdated"] = true;

            return RedirectToAction("InstitutionTypesView", "InstitutionTypes");
        }

        //Delete Page
        [HttpPost]
        public ActionResult InstitutionTypeDelete(int iInstitutionTypeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iInstitutionTypeID == 0)
            {
                return RedirectToAction("InstitutionTypesView", "InstitutionTypes");
            }

            clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();
            clsInstitutionTypesManager.removeInstitutionTypeByID(iInstitutionTypeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsInstitutionTypeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfInstitutionTypeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblInstitutionTypes.Any(InstitutionType => InstitutionType.strTitle.ToLower() == strTitle.ToLower() && InstitutionType.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}