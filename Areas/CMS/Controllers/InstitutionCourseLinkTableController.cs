﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models.InstitutionCourseLinkTable;
using CareerWizCMS.Model_Manager;
using CareerWiz;

namespace CareerWizCMS.Controllers
{
    public class InstitutionCourseLinkTableController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();

        // GET: InstitutionCourseLinkTable
        public ActionResult InstitutionCourseLinkTableView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionCourseLinkTableView clsInstitutionCourseLinkTableView = new clsInstitutionCourseLinkTableView();
            clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
            clsInstitutionCourseLinkTableView.lstInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.getAllInstitutionCourseLinkTableList();

            return View(clsInstitutionCourseLinkTableView);
        }

        public ActionResult InstitutionCourseLinkTableAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionCourseLinkTableAdd clsInstitutionCourseLinkTableAdd = new clsInstitutionCourseLinkTableAdd();
            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsCoursesManager clsCoursesManager = new clsCoursesManager();
            
            clsInstitutionCourseLinkTableAdd.clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();

            clsInstitutionCourseLinkTableAdd.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
            clsInstitutionCourseLinkTableAdd.lstCourses = clsCoursesManager.getAllCoursesList();

            return View(clsInstitutionCourseLinkTableAdd);
        }

        //Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionCourseLinkTableAdd(clsInstitutionCourseLinkTableAdd clsInstitutionCourseLinkTableAdd)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool dDoesRecordExist = db.tblInstitutionCourseLinkTable.Any(InstitutionCourseLinkTable => InstitutionCourseLinkTable.iInstitutionID == clsInstitutionCourseLinkTableAdd.clsInstitutionCourseLinkTable.iInstitutionID
                                    && InstitutionCourseLinkTable.iCourseID == clsInstitutionCourseLinkTableAdd.clsInstitutionCourseLinkTable.iCourseID
                                    && InstitutionCourseLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsInstitutionCourseLinkTableRecordExists"] = true;
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
                clsCoursesManager clsCoursesManager = new clsCoursesManager();

                clsInstitutionCourseLinkTableAdd.clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();
                clsInstitutionCourseLinkTableAdd.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();

                clsInstitutionCourseLinkTableAdd.lstCourses = clsCoursesManager.getAllCoursesList();
                return View(clsInstitutionCourseLinkTableAdd);
            }

            clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
            clsInstitutionCourseLinkTableManager.saveInstitutionCourseLinkTable(clsInstitutionCourseLinkTableAdd.clsInstitutionCourseLinkTable);

            //Add successful / notification
            TempData["bIsInstitutionCourseLinkTableAdded"] = true;

            return RedirectToAction("InstitutionCourseLinkTableView", "InstitutionCourseLinkTable");
        }

        //Edit
        public ActionResult InstitutionCourseLinkTableEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("InstitutionCourseLinkTableView", "InstitutionCourseLinkTable");
            }

            clsInstitutionCourseLinkTableEdit clsInstitutionCourseLinkTableEdit = new clsInstitutionCourseLinkTableEdit();
            clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
            clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.getInstitutionCourseLinkTableByID(id);

            clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
            clsCoursesManager clsCoursesManager = new clsCoursesManager();

            clsInstitutionCourseLinkTableEdit.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
            clsInstitutionCourseLinkTableEdit.lstCourses = clsCoursesManager.getAllCoursesList();

            return View(clsInstitutionCourseLinkTableEdit);
        }

        //Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InstitutionCourseLinkTableEdit(clsInstitutionCourseLinkTableEdit clsInstitutionCourseLinkTableEdit)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();

            bool dDoesRecordExist = db.tblInstitutionCourseLinkTable.Any(InstitutionCourseLinkTable => InstitutionCourseLinkTable.iInstitutionID == clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable.iInstitutionID
                                    && InstitutionCourseLinkTable.iCourseID == clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable.iCourseID
                                    && InstitutionCourseLinkTable.bIsDeleted == false);
            if (dDoesRecordExist == true)
            {
                TempData["bIsInstitutionCourseLinkTableRecordExists"] = true;
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();
                clsCoursesManager clsCoursesManager = new clsCoursesManager();

                clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.getInstitutionCourseLinkTableByID(clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable.iInstitutionCourseLinkTableID);

                clsInstitutionCourseLinkTableEdit.lstInstitutions = clsInstitutionsManager.getAllInstitutionsList();
                clsInstitutionCourseLinkTableEdit.lstCourses = clsCoursesManager.getAllCoursesList();
                return View(clsInstitutionCourseLinkTableEdit);
            }
            
            clsInstitutionCourseLinkTableManager.saveInstitutionCourseLinkTable(clsInstitutionCourseLinkTableEdit.clsInstitutionCourseLinkTable);

            //Add successful / notification
            TempData["bIsInstitutionCourseLinkTableUpdated"] = true;

            return RedirectToAction("InstitutionCourseLinkTableView", "InstitutionCourseLinkTable");
        }

        //Delete
        [HttpPost]
        public ActionResult InstitutionCourseLinkTableDelete(int iInstitutionCourseLinkTableID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iInstitutionCourseLinkTableID == 0)
            {
                return RedirectToAction("InstitutionCourseLinkTableView", "InstitutionCourseLinkTable");
            }

            clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
            clsInstitutionCourseLinkTableManager.removeInstitutionCourseLinkTableByID(iInstitutionCourseLinkTableID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsInstitutionCourseLinkTableDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}