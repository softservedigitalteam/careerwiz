﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsInstitutions
    {
        public int iInstitutionID { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public DateTime? dtEdited { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfInstitutionExists", "Institutions", HttpMethod = "POST", ErrorMessage = "Institution already exists")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strDescriptionOverview { get; set; }

        [Required(ErrorMessage = "Institution is required")]
        public int iInstitutionTypeID { get; set; }
        public string strContactDetails { get; set; }
        public string strVisionMission { get; set; }
        public string strCampusLife { get; set; }
        public string strImportantDatesInformation { get; set; }
        public string strInstitutionURL { get; set; }
        public string strImagePath { get; set; }
        public string strImageName { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsInstitutionCourseLinkTable> lstInstitutionCourseLinkTable { get; set; }
        public List<clsInstitutionIndustrySectorLinkTable> lstInstitutionIndustrySectorLinkTable { get; set; }
        public clsInstitutionTypes clsInstitutionType { get; set; }

    }
}
