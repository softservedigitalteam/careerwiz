﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsIndustrySectorCareerLinkTable
    {
        public int iIndustrySectorCareerLinkTableID { get; set; }

        [Required(ErrorMessage = "Industry Sector is required")]
        public int iIndustrySectorID { get; set; }

        [Required(ErrorMessage = "Career is required")]
        public int iCareerID { get; set; }

        public bool bIsDeleted { get; set; }
        public clsCareers clsCareer { get; set; }
        public clsIndustrySectors clsIndustrySector { get; set; }
    }
}
