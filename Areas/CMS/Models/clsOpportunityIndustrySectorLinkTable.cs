﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsOpportunityIndustrySectorLinkTable
    {
        public int iOpportunityIndustrySectorLinkTableID { get; set; }

        [Required(ErrorMessage = "Opportunity is required")]
        public int iOpportunityID { get; set; }

        [Required(ErrorMessage = "Industry Sector is required")]
        public int iIndustrySectorID { get; set; }

        public bool bIsDeleted { get; set; }

        public clsIndustrySectors clsIndustrySector { get; set; }
        public clsOpportunities clsOpportunity { get; set; }
    }
}
