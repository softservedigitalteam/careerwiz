﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsInstitutionCourseLinkTable
    {
        public int iInstitutionCourseLinkTableID { get; set; }

        [Required(ErrorMessage = "Institution is required")]
        public int iInstitutionID { get; set; }

        [Required(ErrorMessage = "Course is required")]
        public int iCourseID { get; set; }

        public bool bIsDeleted { get; set; }

        public clsCourses clsCourse { get; set; }
        public clsInstitutions clsInstitution { get; set; }
    }
}
