﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareerWizCMS.Models
{
    public class clsOpportunities
    {
        public int iOpportunityID { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsOpportunityIndustrySectorLinkTable> lstOpportunityIndustrySectorLinkTable { get; set; }
        public List<clsOpportunityPreferenceLinkTable> lstOpportunityPreferenceLinkTable { get; set; }

    }
}
