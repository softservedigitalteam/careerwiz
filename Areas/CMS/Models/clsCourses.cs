﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsCourses
    {
        public int iCourseID { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public DateTime? dtEdited { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfCourseExists", "Courses", HttpMethod = "POST", ErrorMessage = "Course title already exists")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsInstitutionCourseLinkTable> lstInstitutionCourseLinkTable { get; set; }

    }
}
