﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace CareerWizCMS.Models
{
    public class clsUserPreferenceLinkTable
    {
        public int iUserPreferenceLinkTableID { get; set; }

        [Required(ErrorMessage = "User is required")]
        public int iUserID { get; set; }

        [Required(ErrorMessage = "Preference is required")]
        public int iPreferenceID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsPreferences clsPreference { get; set; }
        public clsUsers clsUser { get; set; }
    }
}
