﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsStudents
    {
        public int iStudentID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public bool bIsPaidSubscription { get; set; }
        public DateTime dtDateOfBirth { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strGender { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Nationality must be at least 2 characters long")]

        public string strNationality { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Region must be at least 2 characters long")]
        public string strRegion { get; set; }
        public bool bHigherEducation { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Current Institution must be at least 2 characters long")]
        public string strInstitutionName { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Level of study must be at least 2 characters long")]
        public string strCourse { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(500, MinimumLength = 2, ErrorMessage = "Models must be at least 2 characters long")]
        public string strModules { get; set; }

        public bool bIsDeleted { get; set; }
        public List<clsUsers> lstUsers { get; set; }
    }
}
