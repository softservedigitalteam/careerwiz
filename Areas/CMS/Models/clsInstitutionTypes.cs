﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsInstitutionTypes
    {
        public int iInstitutionTypeID { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public DateTime? dtEdited { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfInstitutionTypeExists", "InstitutionTypes", HttpMethod = "POST", ErrorMessage = "Institution Type already exists")]
        public string strTitle { get; set; }

        public bool bIsDeleted { get; set; }

        public List<clsInstitutions> lstInstitutions { get; set; }
    }
}
