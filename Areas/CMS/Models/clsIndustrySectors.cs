﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsIndustrySectors
    {
        public int iIndustrySectorID { get; set; }
        public int iAddedBy { get; set; }
        public DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public DateTime? dtEdited { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfIndustrySectorExists", "IndustrySectors", HttpMethod = "POST", ErrorMessage = "Industry Sector already exists")]
        public string strTitle { get; set; }

        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsIndustrySectorCareerLinkTable> lstIndustrySectorCareerLinkTable { get; set; }
        public List<clsInstitutionIndustrySectorLinkTable> lstInstitutionIndustrySectorLinkTable { get; set; }
        public List<clsOpportunityIndustrySectorLinkTable> lstOpportunityIndustrySectorLinkTable { get; set; }

    }
}
