﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CareerWizCMS.Models
{
    public class clsOpportunityPreferenceLinkTable
    {
        public int iOpportunityPreferenceLinkTableID { get; set; }

        [Required(ErrorMessage = "Opportunity is required")]
        public int iOpportunityID { get; set; }

        [Required(ErrorMessage = "Preference is required")]
        public int iPreferenceID { get; set; }

        public bool bIsDeleted { get; set; }

        public clsOpportunities clsOpportunity { get; set; }
        public clsPreferences clsPreference { get; set; }
    }
}
