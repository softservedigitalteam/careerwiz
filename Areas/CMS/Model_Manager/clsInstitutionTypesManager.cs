﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;


namespace CareerWizCMS.Model_Manager
{
    public class clsInstitutionTypesManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsInstitutionTypes> getAllInstitutionTypesList()
        {
            List<clsInstitutionTypes> lstInstitutionTypes = new List<clsInstitutionTypes>();
            var lstGetInstitutionTypesList = db.tblInstitutionTypes.Where(InstitutionType => InstitutionType.bIsDeleted == false).ToList();

            if (lstGetInstitutionTypesList.Count > 0)
            {
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager(); //CMS User Manager

                foreach (var item in lstGetInstitutionTypesList)
                {
                    clsInstitutionTypes clsInstitutionType = new clsInstitutionTypes();

                    clsInstitutionType.iInstitutionTypeID = item.iInstitutionTypeID;
                    clsInstitutionType.dtAdded = item.dtAdded;
                    clsInstitutionType.iAddedBy = item.iAddedBy;
                    clsInstitutionType.dtEdited = item.dtEdited;
                    clsInstitutionType.iEditedBy = item.iEditedBy;

                    clsInstitutionType.strTitle = item.strTitle;
                    clsInstitutionType.bIsDeleted = item.bIsDeleted;

                    clsInstitutionType.lstInstitutions = new List<clsInstitutions>();

                    if (item.tblInstitutions.Count > 0)
                    {
                        foreach (var InstitutionItem in item.tblInstitutions)
                        {
                            clsInstitutions clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(InstitutionItem);
                            clsInstitutionType.lstInstitutions.Add(clsInstitution);
                        }
                    }

                    lstInstitutionTypes.Add(clsInstitutionType);
                }
            }

            return lstInstitutionTypes;
        }

        //Get
        public clsInstitutionTypes getInstitutionTypeByID(int iInstitutionTypeID)
        {
            clsInstitutionTypes clsInstitutionType = null;
            tblInstitutionTypes tblInstitutionType = db.tblInstitutionTypes.FirstOrDefault(InstitutionType => InstitutionType.iInstitutionTypeID == iInstitutionTypeID && InstitutionType.bIsDeleted == false);

            if (tblInstitutionType != null)
            {
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager(); //CMS User Manager
                clsUsersManager clsUsersManager = new clsUsersManager(); //User Manager

                clsInstitutionType = new clsInstitutionTypes();
                clsInstitutionType.iInstitutionTypeID = tblInstitutionType.iInstitutionTypeID;
                clsInstitutionType.dtAdded = tblInstitutionType.dtAdded;
                clsInstitutionType.iAddedBy = tblInstitutionType.iAddedBy;
                clsInstitutionType.dtEdited = tblInstitutionType.dtEdited;
                clsInstitutionType.iEditedBy = tblInstitutionType.iEditedBy;

                clsInstitutionType.strTitle = tblInstitutionType.strTitle;
                clsInstitutionType.bIsDeleted = tblInstitutionType.bIsDeleted;

                clsInstitutionType.lstInstitutions = new List<clsInstitutions>();

                if (tblInstitutionType.tblInstitutions.Count > 0)
                {
                    foreach (var InstitutionItem in tblInstitutionType.tblInstitutions)
                    {
                        clsInstitutions clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(InstitutionItem);
                        clsInstitutionType.lstInstitutions.Add(clsInstitution);
                    }
                }
            }

            return clsInstitutionType;
        }

        //Save
        public void saveInstitutionType(clsInstitutionTypes clsInstitutionType)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblInstitutionTypes tblInstitutionType = new tblInstitutionTypes();

                tblInstitutionType.iInstitutionTypeID = clsInstitutionType.iInstitutionTypeID;

                tblInstitutionType.strTitle = clsInstitutionType.strTitle;
                tblInstitutionType.bIsDeleted = clsInstitutionType.bIsDeleted;

                //Add
                if (tblInstitutionType.iInstitutionTypeID == 0)
                {
                    tblInstitutionType.dtAdded = DateTime.Now;
                    tblInstitutionType.iAddedBy = clsCMSUser.iCMSUserID;
                    tblInstitutionType.dtEdited = DateTime.Now;
                    tblInstitutionType.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblInstitutionTypes.Add(tblInstitutionType);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblInstitutionType.dtAdded = clsInstitutionType.dtAdded;
                    tblInstitutionType.iAddedBy = clsInstitutionType.iAddedBy;
                    tblInstitutionType.dtEdited = DateTime.Now;
                    tblInstitutionType.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblInstitutionTypes>().AddOrUpdate(tblInstitutionType);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeInstitutionTypeByID(int iInstitutionTypeID)
        {
            tblInstitutionTypes tblInstitutionType = db.tblInstitutionTypes.Find(iInstitutionTypeID);
            if (tblInstitutionType != null)
            {
                tblInstitutionType.bIsDeleted = true;
                db.Entry(tblInstitutionType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfInstitutionTypeExists(int iInstitutionTypeID)
        {
            bool bInstitutionTypeExists = db.tblInstitutionTypes.Any(InstitutionType => InstitutionType.iInstitutionTypeID == iInstitutionTypeID && InstitutionType.bIsDeleted == false);
            return bInstitutionTypeExists;
        }

        //Convert database table to class
        public clsInstitutionTypes convertInstitutionTypesTableToClass(tblInstitutionTypes tblInstitutionType)
        {
            clsInstitutionTypes clsInstitutionType = new clsInstitutionTypes();

            clsInstitutionType.iInstitutionTypeID = tblInstitutionType.iInstitutionTypeID;
            clsInstitutionType.dtAdded = tblInstitutionType.dtAdded;
            clsInstitutionType.iAddedBy = tblInstitutionType.iAddedBy;
            clsInstitutionType.dtEdited = tblInstitutionType.dtEdited;
            clsInstitutionType.iEditedBy = tblInstitutionType.iEditedBy;

            clsInstitutionType.strTitle = tblInstitutionType.strTitle;
            clsInstitutionType.bIsDeleted = tblInstitutionType.bIsDeleted;

            return clsInstitutionType;
        }
    }
}
