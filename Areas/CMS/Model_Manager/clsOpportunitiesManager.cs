﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsOpportunitiesManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsOpportunities> getAllOpportunitiesList()
        {
            List<clsOpportunities> lstOpportunities = new List<clsOpportunities>();
            var lstGetOpportunitiesList = db.tblOpportunities.Where(Opportunity => Opportunity.bIsDeleted == false).ToList();

            if (lstGetOpportunitiesList.Count > 0)
            {
                clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
                clsOpportunityPreferenceLinkTableManager clsOpportunityPreferenceLinkTableManager = new clsOpportunityPreferenceLinkTableManager();

                foreach (var item in lstGetOpportunitiesList)
                {
                    clsOpportunities clsOpportunity = new clsOpportunities();

                    clsOpportunity.iOpportunityID = item.iOpportunityID;
                    clsOpportunity.dtAdded = item.dtAdded;
                    clsOpportunity.iAddedBy = item.iAddedBy;
                    clsOpportunity.dtEdited = item.dtEdited;
                    clsOpportunity.iEditedBy = item.iEditedBy;

                    clsOpportunity.strTitle = item.strTitle;
                    clsOpportunity.strDescription = item.strDescription;
                    clsOpportunity.bIsDeleted = item.bIsDeleted;

                    clsOpportunity.lstOpportunityIndustrySectorLinkTable = new List<clsOpportunityIndustrySectorLinkTable>();
                    clsOpportunity.lstOpportunityPreferenceLinkTable = new List<clsOpportunityPreferenceLinkTable>();

                    if (item.tblOpportunityIndustrySectorLinkTable.Count > 0)
                    {
                        foreach (var OpportunityIndustrySectorLinkTableItem in item.tblOpportunityIndustrySectorLinkTable)
                        {
                            clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.convertOpportunityIndustrySectorLinkTableTableToClass(OpportunityIndustrySectorLinkTableItem);
                            clsOpportunity.lstOpportunityIndustrySectorLinkTable.Add(clsOpportunityIndustrySectorLinkTable);
                        }
                    }
                    if (item.tblOpportunityPreferenceLinkTable.Count > 0)
                    {
                        foreach (var OpportunityPreferenceLinkTableItem in item.tblOpportunityPreferenceLinkTable)
                        {
                            clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = clsOpportunityPreferenceLinkTableManager.convertOpportunityPreferenceLinkTableTableToClass(OpportunityPreferenceLinkTableItem);
                            clsOpportunity.lstOpportunityPreferenceLinkTable.Add(clsOpportunityPreferenceLinkTable);
                        }
                    }

                    lstOpportunities.Add(clsOpportunity);
                }
            }

            return lstOpportunities;
        }

        //Get
        public clsOpportunities getOpportunityByID(int iOpportunityID)
        {
            clsOpportunities clsOpportunity = null;
            tblOpportunities tblOpportunity = db.tblOpportunities.FirstOrDefault(Opportunity => Opportunity.iOpportunityID == iOpportunityID && Opportunity.bIsDeleted == false);

            if (tblOpportunity != null)
            {
                clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();
                clsOpportunityPreferenceLinkTableManager clsOpportunityPreferenceLinkTableManager = new clsOpportunityPreferenceLinkTableManager();

                clsOpportunity = new clsOpportunities();
                clsOpportunity.iOpportunityID = tblOpportunity.iOpportunityID;
                clsOpportunity.dtAdded = tblOpportunity.dtAdded;
                clsOpportunity.iAddedBy = tblOpportunity.iAddedBy;
                clsOpportunity.dtEdited = tblOpportunity.dtEdited;
                clsOpportunity.iEditedBy = tblOpportunity.iEditedBy;

                clsOpportunity.strTitle = tblOpportunity.strTitle;
                clsOpportunity.strDescription = tblOpportunity.strDescription;
                clsOpportunity.bIsDeleted = tblOpportunity.bIsDeleted;

                clsOpportunity.lstOpportunityIndustrySectorLinkTable = new List<clsOpportunityIndustrySectorLinkTable>();

                if (tblOpportunity.tblOpportunityIndustrySectorLinkTable.Count > 0)
                {
                    foreach (var OpportunityIndustrySectorLinkTableItem in tblOpportunity.tblOpportunityIndustrySectorLinkTable)
                    {
                        clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.convertOpportunityIndustrySectorLinkTableTableToClass(OpportunityIndustrySectorLinkTableItem);
                        clsOpportunity.lstOpportunityIndustrySectorLinkTable.Add(clsOpportunityIndustrySectorLinkTable);
                    }
                }
                if (tblOpportunity.tblOpportunityPreferenceLinkTable.Count > 0)
                {
                    foreach (var OpportunityPreferenceLinkTableItem in tblOpportunity.tblOpportunityPreferenceLinkTable)
                    {
                        clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = clsOpportunityPreferenceLinkTableManager.convertOpportunityPreferenceLinkTableTableToClass(OpportunityPreferenceLinkTableItem);
                        clsOpportunity.lstOpportunityPreferenceLinkTable.Add(clsOpportunityPreferenceLinkTable);
                    }
                }
            }
            return clsOpportunity;
        }

        //Save
        public void saveOpportunity(clsOpportunities clsOpportunity)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblOpportunities tblOpportunity = new tblOpportunities();

                tblOpportunity.iOpportunityID = clsOpportunity.iOpportunityID;

                tblOpportunity.strTitle = clsOpportunity.strTitle;
                tblOpportunity.strDescription = clsOpportunity.strDescription;

                tblOpportunity.bIsDeleted = clsOpportunity.bIsDeleted;

                //Add
                if (tblOpportunity.iOpportunityID == 0)
                {
                    tblOpportunity.dtAdded = DateTime.Now;
                    tblOpportunity.iAddedBy = clsCMSUser.iCMSUserID;
                    tblOpportunity.dtEdited = DateTime.Now;
                    tblOpportunity.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblOpportunities.Add(tblOpportunity);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblOpportunity.dtAdded = clsOpportunity.dtAdded;
                    tblOpportunity.iAddedBy = clsOpportunity.iAddedBy;
                    tblOpportunity.dtEdited = DateTime.Now;
                    tblOpportunity.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblOpportunities>().AddOrUpdate(tblOpportunity);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeOpportunityByID(int iOpportunityID)
        {
            tblOpportunities tblOpportunity = db.tblOpportunities.Find(iOpportunityID);
            if (tblOpportunity != null)
            {
                tblOpportunity.bIsDeleted = true;
                db.Entry(tblOpportunity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfOpportunityExists(int iOpportunityID)
        {
            bool bOpportunityExists = db.tblOpportunities.Any(Opportunity => Opportunity.iOpportunityID == iOpportunityID && Opportunity.bIsDeleted == false);
            return bOpportunityExists;
        }

        //Convert database table to class
        public clsOpportunities convertOpportunitiesTableToClass(tblOpportunities tblOpportunity)
        {
            clsOpportunities clsOpportunity = new clsOpportunities();

            clsOpportunity.iOpportunityID = tblOpportunity.iOpportunityID;
            clsOpportunity.dtAdded = tblOpportunity.dtAdded;
            clsOpportunity.iAddedBy = tblOpportunity.iAddedBy;
            clsOpportunity.dtEdited = tblOpportunity.dtEdited;
            clsOpportunity.iEditedBy = tblOpportunity.iEditedBy;

            clsOpportunity.strTitle = tblOpportunity.strTitle;
            clsOpportunity.strDescription = tblOpportunity.strDescription;

            clsOpportunity.bIsDeleted = tblOpportunity.bIsDeleted;

            return clsOpportunity;
        }
    }
}
