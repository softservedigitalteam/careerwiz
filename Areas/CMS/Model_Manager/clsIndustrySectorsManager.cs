﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsIndustrySectorsManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsIndustrySectors> getAllIndustrySectorsList()
        {
            List<clsIndustrySectors> lstIndustrySectors = new List<clsIndustrySectors>();
            var lstGetIndustrySectorsList = db.tblIndustrySectors.Where(IndustrySector => IndustrySector.bIsDeleted == false).ToList();

            if (lstGetIndustrySectorsList.Count > 0)
            {
                clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
                clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
                clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();

                foreach (var item in lstGetIndustrySectorsList)
                {
                    clsIndustrySectors clsIndustrySector = new clsIndustrySectors();

                    clsIndustrySector.iIndustrySectorID = item.iIndustrySectorID;
                    clsIndustrySector.dtAdded = item.dtAdded;
                    clsIndustrySector.iAddedBy = item.iAddedBy;
                    clsIndustrySector.dtEdited = item.dtEdited;
                    clsIndustrySector.iEditedBy = item.iEditedBy;

                    clsIndustrySector.strTitle = item.strTitle;
                    clsIndustrySector.strDescription = item.strDescription;
                    clsIndustrySector.bIsDeleted = item.bIsDeleted;

                    clsIndustrySector.lstIndustrySectorCareerLinkTable = new List<clsIndustrySectorCareerLinkTable>();
                    clsIndustrySector.lstInstitutionIndustrySectorLinkTable = new List<clsInstitutionIndustrySectorLinkTable>();
                    clsIndustrySector.lstOpportunityIndustrySectorLinkTable = new List<clsOpportunityIndustrySectorLinkTable>();

                    if (item.tblIndustrySectorCareerLinkTable.Count > 0)
                    {
                        foreach (var IndustrySectorCareerLinkTableItem in item.tblIndustrySectorCareerLinkTable)
                        {
                            clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.convertIndustrySectorCareerLinkTableTableToClass(IndustrySectorCareerLinkTableItem);
                            clsIndustrySector.lstIndustrySectorCareerLinkTable.Add(clsIndustrySectorCareerLinkTable);
                        }
                    }
                    if (item.tblInstitutionIndustrySectorLinkTable.Count > 0)
                    {
                        foreach (var InstitutionIndustrySectorLinkTableItem in item.tblInstitutionIndustrySectorLinkTable)
                        {
                            clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.convertInstitutionIndustrySectorLinkTableTableToClass(InstitutionIndustrySectorLinkTableItem);
                            clsIndustrySector.lstInstitutionIndustrySectorLinkTable.Add(clsInstitutionIndustrySectorLinkTable);
                        }
                    }
                    if (item.tblOpportunityIndustrySectorLinkTable.Count > 0)
                    {
                        foreach (var OpportunityIndustrySectorLinkTableItem in item.tblOpportunityIndustrySectorLinkTable)
                        {
                            clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.convertOpportunityIndustrySectorLinkTableTableToClass(OpportunityIndustrySectorLinkTableItem);
                            clsIndustrySector.lstOpportunityIndustrySectorLinkTable.Add(clsOpportunityIndustrySectorLinkTable);
                        }
                    }

                    lstIndustrySectors.Add(clsIndustrySector);
                }
            }

            return lstIndustrySectors;
        }

        //Get
        public clsIndustrySectors getIndustrySectorByID(int iIndustrySectorID)
        {
            clsIndustrySectors clsIndustrySector = null;
            tblIndustrySectors tblIndustrySector = db.tblIndustrySectors.FirstOrDefault(IndustrySector => IndustrySector.iIndustrySectorID == iIndustrySectorID && IndustrySector.bIsDeleted == false);

            if (tblIndustrySector != null)
            {
                clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
                clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
                clsOpportunityIndustrySectorLinkTableManager clsOpportunityIndustrySectorLinkTableManager = new clsOpportunityIndustrySectorLinkTableManager();

                clsIndustrySector = new clsIndustrySectors();
                clsIndustrySector.iIndustrySectorID = tblIndustrySector.iIndustrySectorID;
                clsIndustrySector.dtAdded = tblIndustrySector.dtAdded;
                clsIndustrySector.iAddedBy = tblIndustrySector.iAddedBy;
                clsIndustrySector.dtEdited = tblIndustrySector.dtEdited;
                clsIndustrySector.iEditedBy = tblIndustrySector.iEditedBy;

                clsIndustrySector.strTitle = tblIndustrySector.strTitle;
                clsIndustrySector.strDescription = tblIndustrySector.strDescription;
                clsIndustrySector.bIsDeleted = tblIndustrySector.bIsDeleted;

                clsIndustrySector.lstIndustrySectorCareerLinkTable = new List<clsIndustrySectorCareerLinkTable>();

                if (tblIndustrySector.tblIndustrySectorCareerLinkTable.Count > 0)
                {
                    foreach (var IndustrySectorCareerLinkTableItem in tblIndustrySector.tblIndustrySectorCareerLinkTable)
                    {
                        clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.convertIndustrySectorCareerLinkTableTableToClass(IndustrySectorCareerLinkTableItem);
                        clsIndustrySector.lstIndustrySectorCareerLinkTable.Add(clsIndustrySectorCareerLinkTable);
                    }
                }
                if (tblIndustrySector.tblInstitutionIndustrySectorLinkTable.Count > 0)
                {
                    foreach (var InstitutionIndustrySectorLinkTableItem in tblIndustrySector.tblInstitutionIndustrySectorLinkTable)
                    {
                        clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.convertInstitutionIndustrySectorLinkTableTableToClass(InstitutionIndustrySectorLinkTableItem);
                        clsIndustrySector.lstInstitutionIndustrySectorLinkTable.Add(clsInstitutionIndustrySectorLinkTable);
                    }
                }
                if (tblIndustrySector.tblOpportunityIndustrySectorLinkTable.Count > 0)
                {
                    foreach (var OpportunityIndustrySectorLinkTableItem in tblIndustrySector.tblOpportunityIndustrySectorLinkTable)
                    {
                        clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = clsOpportunityIndustrySectorLinkTableManager.convertOpportunityIndustrySectorLinkTableTableToClass(OpportunityIndustrySectorLinkTableItem);
                        clsIndustrySector.lstOpportunityIndustrySectorLinkTable.Add(clsOpportunityIndustrySectorLinkTable);
                    }
                }
            }

            return clsIndustrySector;
        }

        //Save
        public void saveIndustrySector(clsIndustrySectors clsIndustrySector)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblIndustrySectors tblIndustrySector = new tblIndustrySectors();

                tblIndustrySector.iIndustrySectorID = clsIndustrySector.iIndustrySectorID;

                tblIndustrySector.strTitle = clsIndustrySector.strTitle;
                tblIndustrySector.strDescription = clsIndustrySector.strDescription;
                tblIndustrySector.bIsDeleted = clsIndustrySector.bIsDeleted;

                //Add
                if (tblIndustrySector.iIndustrySectorID == 0)
                {
                    tblIndustrySector.dtAdded = DateTime.Now;
                    tblIndustrySector.iAddedBy = clsCMSUser.iCMSUserID;
                    tblIndustrySector.dtEdited = DateTime.Now;
                    tblIndustrySector.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblIndustrySectors.Add(tblIndustrySector);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblIndustrySector.dtAdded = clsIndustrySector.dtAdded;
                    tblIndustrySector.iAddedBy = clsIndustrySector.iAddedBy;
                    tblIndustrySector.dtEdited = DateTime.Now;
                    tblIndustrySector.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblIndustrySectors>().AddOrUpdate(tblIndustrySector);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeIndustrySectorByID(int iIndustrySectorID)
        {
            tblIndustrySectors tblIndustrySector = db.tblIndustrySectors.Find(iIndustrySectorID);
            if (tblIndustrySector != null)
            {
                tblIndustrySector.bIsDeleted = true;
                db.Entry(tblIndustrySector).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfIndustrySectorExists(int iIndustrySectorID)
        {
            bool bIndustrySectorExists = db.tblIndustrySectors.Any(IndustrySector => IndustrySector.iIndustrySectorID == iIndustrySectorID && IndustrySector.bIsDeleted == false);
            return bIndustrySectorExists;
        }

        //Convert database table to class
        public clsIndustrySectors convertIndustrySectorsTableToClass(tblIndustrySectors tblIndustrySector)
        {
            clsIndustrySectors clsIndustrySector = new clsIndustrySectors();

            clsIndustrySector.iIndustrySectorID = tblIndustrySector.iIndustrySectorID;
            clsIndustrySector.dtAdded = tblIndustrySector.dtAdded;
            clsIndustrySector.iAddedBy = tblIndustrySector.iAddedBy;
            clsIndustrySector.dtEdited = tblIndustrySector.dtEdited;
            clsIndustrySector.iEditedBy = tblIndustrySector.iEditedBy;

            clsIndustrySector.strTitle = tblIndustrySector.strTitle;
            clsIndustrySector.strDescription = tblIndustrySector.strDescription;
            clsIndustrySector.bIsDeleted = tblIndustrySector.bIsDeleted;

            return clsIndustrySector;
        }
    }
}
