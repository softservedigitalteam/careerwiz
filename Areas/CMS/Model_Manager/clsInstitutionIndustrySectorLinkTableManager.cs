﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsInstitutionIndustrySectorLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsInstitutionIndustrySectorLinkTable> getAllInstitutionIndustrySectorLinkTableList()
        {
            List<clsInstitutionIndustrySectorLinkTable> lstInstitutionIndustrySectorLinkTable = new List<clsInstitutionIndustrySectorLinkTable>();
            var lstGetInstitutionIndustrySectorLinkTableList = db.tblInstitutionIndustrySectorLinkTable.Where(InstitutionIndustrySectorLinkTable => InstitutionIndustrySectorLinkTable.bIsDeleted == false).ToList();

            if (lstGetInstitutionIndustrySectorLinkTableList.Count > 0)
            {
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

                foreach (var item in lstGetInstitutionIndustrySectorLinkTableList)
                {
                    clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();

                    clsInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID = item.iInstitutionIndustrySectorLinkTableID;
                    clsInstitutionIndustrySectorLinkTable.iInstitutionID = item.iInstitutionID;
                    clsInstitutionIndustrySectorLinkTable.iIndustrySectorID = item.iIndustrySectorID;

                    clsInstitutionIndustrySectorLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblIndustrySectors != null)
                        clsInstitutionIndustrySectorLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(item.tblIndustrySectors);
                    if (item.tblInstitutions != null)
                        clsInstitutionIndustrySectorLinkTable.clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(item.tblInstitutions);

                    lstInstitutionIndustrySectorLinkTable.Add(clsInstitutionIndustrySectorLinkTable);
                }
            }

            return lstInstitutionIndustrySectorLinkTable;
        }

        //Get
        public clsInstitutionIndustrySectorLinkTable getInstitutionIndustrySectorLinkTableByID(int iInstitutionIndustrySectorLinkTableID)
        {
            clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = null;
            tblInstitutionIndustrySectorLinkTable tblInstitutionIndustrySectorLinkTable = db.tblInstitutionIndustrySectorLinkTable.FirstOrDefault(InstitutionIndustrySectorLinkTable => InstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID == iInstitutionIndustrySectorLinkTableID && InstitutionIndustrySectorLinkTable.bIsDeleted == false);

            if (tblInstitutionIndustrySectorLinkTable != null)
            {
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager(); //IndustrySector Manager
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager(); //Institutions Manger

                clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();

                clsInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID = tblInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID;
                clsInstitutionIndustrySectorLinkTable.iInstitutionID = tblInstitutionIndustrySectorLinkTable.iInstitutionID;
                clsInstitutionIndustrySectorLinkTable.iIndustrySectorID = tblInstitutionIndustrySectorLinkTable.iIndustrySectorID;

                clsInstitutionIndustrySectorLinkTable.bIsDeleted = tblInstitutionIndustrySectorLinkTable.bIsDeleted;

                if (tblInstitutionIndustrySectorLinkTable.tblIndustrySectors != null)
                    clsInstitutionIndustrySectorLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(tblInstitutionIndustrySectorLinkTable.tblIndustrySectors);
                if (tblInstitutionIndustrySectorLinkTable.tblInstitutions != null)
                    clsInstitutionIndustrySectorLinkTable.clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(tblInstitutionIndustrySectorLinkTable.tblInstitutions);
            }

            return clsInstitutionIndustrySectorLinkTable;
        }

        //Save
        public void saveInstitutionIndustrySectorLinkTable(clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblInstitutionIndustrySectorLinkTable tblInstitutionIndustrySectorLinkTable = new tblInstitutionIndustrySectorLinkTable();

                tblInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID = clsInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID;

                tblInstitutionIndustrySectorLinkTable.iIndustrySectorID = clsInstitutionIndustrySectorLinkTable.iIndustrySectorID;
                tblInstitutionIndustrySectorLinkTable.iInstitutionID = clsInstitutionIndustrySectorLinkTable.iInstitutionID;
                tblInstitutionIndustrySectorLinkTable.iIndustrySectorID = clsInstitutionIndustrySectorLinkTable.iIndustrySectorID;
                tblInstitutionIndustrySectorLinkTable.bIsDeleted = clsInstitutionIndustrySectorLinkTable.bIsDeleted;

                //Add
                if (tblInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID == 0)
                {
                    db.tblInstitutionIndustrySectorLinkTable.Add(tblInstitutionIndustrySectorLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblInstitutionIndustrySectorLinkTable>().AddOrUpdate(tblInstitutionIndustrySectorLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeInstitutionIndustrySectorLinkTableByID(int iInstitutionIndustrySectorLinkTableID)
        {
            tblInstitutionIndustrySectorLinkTable tblInstitutionIndustrySectorLinkTable = db.tblInstitutionIndustrySectorLinkTable.Find(iInstitutionIndustrySectorLinkTableID);
            if (tblInstitutionIndustrySectorLinkTable != null)
            {
                tblInstitutionIndustrySectorLinkTable.bIsDeleted = true;
                db.Entry(tblInstitutionIndustrySectorLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfInstitutionIndustrySectorLinkTableExists(int iInstitutionIndustrySectorLinkTableID)
        {
            bool bInstitutionIndustrySectorLinkTableExists = db.tblInstitutionIndustrySectorLinkTable.Any(InstitutionIndustrySectorLinkTable => InstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID == iInstitutionIndustrySectorLinkTableID && InstitutionIndustrySectorLinkTable.bIsDeleted == false);
            return bInstitutionIndustrySectorLinkTableExists;
        }


        //Convert database table to class
        public clsInstitutionIndustrySectorLinkTable convertInstitutionIndustrySectorLinkTableTableToClass(tblInstitutionIndustrySectorLinkTable tblInstitutionIndustrySectorLinkTable)
        {
            clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();

            clsInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID = tblInstitutionIndustrySectorLinkTable.iInstitutionIndustrySectorLinkTableID;

            clsInstitutionIndustrySectorLinkTable.iIndustrySectorID = tblInstitutionIndustrySectorLinkTable.iIndustrySectorID;
            clsInstitutionIndustrySectorLinkTable.iInstitutionID = tblInstitutionIndustrySectorLinkTable.iInstitutionID;

            clsInstitutionIndustrySectorLinkTable.bIsDeleted = tblInstitutionIndustrySectorLinkTable.bIsDeleted;

            return clsInstitutionIndustrySectorLinkTable;
        }
    }
}
