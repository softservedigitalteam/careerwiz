﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsOpportunityPreferenceLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsOpportunityPreferenceLinkTable> getAllOpportunityPreferenceLinkTableList()
        {
            List<clsOpportunityPreferenceLinkTable> lstOpportunityPreferenceLinkTable = new List<clsOpportunityPreferenceLinkTable>();
            var lstGetOpportunityPreferenceLinkTableList = db.tblOpportunityPreferenceLinkTable.Where(OpportunityPreferenceLinkTable => OpportunityPreferenceLinkTable.bIsDeleted == false).ToList();

            if (lstGetOpportunityPreferenceLinkTableList.Count > 0)
            {
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

                foreach (var item in lstGetOpportunityPreferenceLinkTableList)
                {
                    clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = new clsOpportunityPreferenceLinkTable();

                    clsOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID = item.iOpportunityPreferenceLinkTableID;
                    clsOpportunityPreferenceLinkTable.iOpportunityID = item.iOpportunityID;
                    clsOpportunityPreferenceLinkTable.iPreferenceID = item.iPreferenceID;

                    clsOpportunityPreferenceLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblPreferences != null)
                        clsOpportunityPreferenceLinkTable.clsPreference = clsPreferencesManager.convertPreferencesTableToClass(item.tblPreferences);
                    if (item.tblOpportunities != null)
                        clsOpportunityPreferenceLinkTable.clsOpportunity = clsOpportunitiesManager.convertOpportunitiesTableToClass(item.tblOpportunities);

                    lstOpportunityPreferenceLinkTable.Add(clsOpportunityPreferenceLinkTable);
                }
            }

            return lstOpportunityPreferenceLinkTable;
        }

        //Get
        public clsOpportunityPreferenceLinkTable getOpportunityPreferenceLinkTableByID(int iOpportunityPreferenceLinkTableID)
        {
            clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = null;
            tblOpportunityPreferenceLinkTable tblOpportunityPreferenceLinkTable = db.tblOpportunityPreferenceLinkTable.FirstOrDefault(OpportunityPreferenceLinkTable => OpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID == iOpportunityPreferenceLinkTableID && OpportunityPreferenceLinkTable.bIsDeleted == false);

            if (tblOpportunityPreferenceLinkTable != null)
            {
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager(); //Preference Manager
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager(); //Opportunities Manger

                clsOpportunityPreferenceLinkTable = new clsOpportunityPreferenceLinkTable();

                clsOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID = tblOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID;
                clsOpportunityPreferenceLinkTable.iOpportunityID = tblOpportunityPreferenceLinkTable.iOpportunityID;
                clsOpportunityPreferenceLinkTable.iPreferenceID = tblOpportunityPreferenceLinkTable.iPreferenceID;

                clsOpportunityPreferenceLinkTable.bIsDeleted = tblOpportunityPreferenceLinkTable.bIsDeleted;

                if (tblOpportunityPreferenceLinkTable.tblPreferences != null)
                    clsOpportunityPreferenceLinkTable.clsPreference = clsPreferencesManager.convertPreferencesTableToClass(tblOpportunityPreferenceLinkTable.tblPreferences);
                if (tblOpportunityPreferenceLinkTable.tblOpportunities != null)
                    clsOpportunityPreferenceLinkTable.clsOpportunity = clsOpportunitiesManager.convertOpportunitiesTableToClass(tblOpportunityPreferenceLinkTable.tblOpportunities);
            }

            return clsOpportunityPreferenceLinkTable;
        }

        //Save
        public void saveOpportunityPreferenceLinkTable(clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblOpportunityPreferenceLinkTable tblOpportunityPreferenceLinkTable = new tblOpportunityPreferenceLinkTable();

                tblOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID = clsOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID;

                tblOpportunityPreferenceLinkTable.iPreferenceID = clsOpportunityPreferenceLinkTable.iPreferenceID;
                tblOpportunityPreferenceLinkTable.iOpportunityID = clsOpportunityPreferenceLinkTable.iOpportunityID;
                tblOpportunityPreferenceLinkTable.iPreferenceID = clsOpportunityPreferenceLinkTable.iPreferenceID;
                tblOpportunityPreferenceLinkTable.bIsDeleted = clsOpportunityPreferenceLinkTable.bIsDeleted;

                //Add
                if (tblOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID == 0)
                {
                    db.tblOpportunityPreferenceLinkTable.Add(tblOpportunityPreferenceLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblOpportunityPreferenceLinkTable>().AddOrUpdate(tblOpportunityPreferenceLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeOpportunityPreferenceLinkTableByID(int iOpportunityPreferenceLinkTableID)
        {
            tblOpportunityPreferenceLinkTable tblOpportunityPreferenceLinkTable = db.tblOpportunityPreferenceLinkTable.Find(iOpportunityPreferenceLinkTableID);
            if (tblOpportunityPreferenceLinkTable != null)
            {
                tblOpportunityPreferenceLinkTable.bIsDeleted = true;
                db.Entry(tblOpportunityPreferenceLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfOpportunityPreferenceLinkTableExists(int iOpportunityPreferenceLinkTableID)
        {
            bool bOpportunityPreferenceLinkTableExists = db.tblOpportunityPreferenceLinkTable.Any(OpportunityPreferenceLinkTable => OpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID == iOpportunityPreferenceLinkTableID && OpportunityPreferenceLinkTable.bIsDeleted == false);
            return bOpportunityPreferenceLinkTableExists;
        }


        //Convert database table to class
        public clsOpportunityPreferenceLinkTable convertOpportunityPreferenceLinkTableTableToClass(tblOpportunityPreferenceLinkTable tblOpportunityPreferenceLinkTable)
        {
            clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = new clsOpportunityPreferenceLinkTable();

            clsOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID = tblOpportunityPreferenceLinkTable.iOpportunityPreferenceLinkTableID;

            clsOpportunityPreferenceLinkTable.iPreferenceID = tblOpportunityPreferenceLinkTable.iPreferenceID;
            clsOpportunityPreferenceLinkTable.iOpportunityID = tblOpportunityPreferenceLinkTable.iOpportunityID;

            clsOpportunityPreferenceLinkTable.bIsDeleted = tblOpportunityPreferenceLinkTable.bIsDeleted;

            return clsOpportunityPreferenceLinkTable;
        }
    }
}
