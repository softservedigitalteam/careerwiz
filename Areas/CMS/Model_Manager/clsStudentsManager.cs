﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsStudentsManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsStudents> getAllStudentsList()
        {
            List<clsStudents> lstStudents = new List<clsStudents>();
            var lstGetStudentsList = db.tblStudents.Where(Student => Student.bIsDeleted == false).ToList();

            if (lstGetStudentsList.Count > 0)
            {
                clsUsersManager clsUsersManager = new clsUsersManager();
                clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();

                foreach (var item in lstGetStudentsList)
                {
                    clsStudents clsStudent = new clsStudents();

                    clsStudent.iStudentID = item.iStudentID;
                    clsStudent.dtAdded = item.dtAdded;
                    clsStudent.iAddedBy = item.iAddedBy;
                    clsStudent.dtEdited = item.dtEdited;
                    clsStudent.iEditedBy = item.iEditedBy;

                    clsStudent.bIsPaidSubscription = item.bIsPaidSubscription;
                    clsStudent.dtDateOfBirth = item.dtDateOfBirth;
                    clsStudent.strGender = item.strGender;
                    clsStudent.strNationality = item.strNationality;
                    clsStudent.strRegion = item.strRegion;

                    clsStudent.bHigherEducation = item.bHigherEducation;
                    clsStudent.strInstitutionName = item.strInstitutionName;
                    clsStudent.strCourse = item.strCourse;
                    clsStudent.strModules = item.strModules;
                    clsStudent.bIsDeleted = item.bIsDeleted;

                    clsStudent.lstUsers = new List<clsUsers>();

                    if (item.tblUsers.Count > 0)
                    {
                        foreach (var UsersItem in item.tblUsers)
                        {
                            clsUsers clsUsers = clsUsersManager.convertUsersTableToClass(UsersItem);
                            clsStudent.lstUsers.Add(clsUsers);
                        }
                    }

                    lstStudents.Add(clsStudent);
                }
            }

            return lstStudents;
        }

        //Get
        public clsStudents getStudentByID(int iStudentID)
        {
            clsStudents clsStudent = null;
            tblStudents tblStudent = db.tblStudents.FirstOrDefault(Student => Student.iStudentID == iStudentID && Student.bIsDeleted == false);

            if (tblStudent != null)
            {
                clsUsersManager clsUsersManager = new clsUsersManager();
                clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();

                clsStudent = new clsStudents();
                clsStudent.iStudentID = tblStudent.iStudentID;
                clsStudent.dtAdded = tblStudent.dtAdded;
                clsStudent.iAddedBy = tblStudent.iAddedBy;
                clsStudent.dtEdited = tblStudent.dtEdited;
                clsStudent.iEditedBy = tblStudent.iEditedBy;

                clsStudent.bIsPaidSubscription = tblStudent.bIsPaidSubscription;
                clsStudent.dtDateOfBirth = tblStudent.dtDateOfBirth;
                clsStudent.strGender = tblStudent.strGender;
                clsStudent.strNationality = tblStudent.strNationality;
                clsStudent.strRegion = tblStudent.strRegion;

                clsStudent.bHigherEducation = tblStudent.bHigherEducation;
                clsStudent.strInstitutionName = tblStudent.strInstitutionName;
                clsStudent.strCourse = tblStudent.strCourse;
                clsStudent.strModules = tblStudent.strModules;
                clsStudent.bIsDeleted = tblStudent.bIsDeleted;

                clsStudent.lstUsers = new List<clsUsers>();

                if (tblStudent.tblUsers.Count > 0)
                {
                    foreach (var UsersItem in tblStudent.tblUsers)
                    {
                        clsUsers clsUsers = clsUsersManager.convertUsersTableToClass(UsersItem);
                        clsStudent.lstUsers.Add(clsUsers);
                    }
                }
            }
            return clsStudent;
        }

        //Save
        public int saveStudent(clsStudents clsStudent)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblStudents tblStudent = new tblStudents();

                tblStudent.iStudentID = clsStudent.iStudentID;

                tblStudent.bIsPaidSubscription = clsStudent.bIsPaidSubscription;
                tblStudent.dtDateOfBirth = clsStudent.dtDateOfBirth;
                tblStudent.strGender = clsStudent.strGender;
                tblStudent.strNationality = clsStudent.strNationality;
                tblStudent.strRegion = clsStudent.strRegion;

                tblStudent.bHigherEducation = clsStudent.bHigherEducation;
                tblStudent.strInstitutionName = clsStudent.strInstitutionName;
                tblStudent.strCourse = clsStudent.strCourse;
                tblStudent.strModules = clsStudent.strModules;

                tblStudent.bIsDeleted = clsStudent.bIsDeleted;

                //Add
                if (tblStudent.iStudentID == 0)
                {
                    tblStudent.dtAdded = DateTime.Now;
                    tblStudent.iAddedBy = clsCMSUser.iCMSUserID;
                    tblStudent.dtEdited = DateTime.Now;
                    tblStudent.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblStudents.Add(tblStudent);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblStudent.dtAdded = clsStudent.dtAdded;
                    tblStudent.iAddedBy = clsStudent.iAddedBy;
                    tblStudent.dtEdited = DateTime.Now;
                    tblStudent.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblStudents>().AddOrUpdate(tblStudent);
                    db.SaveChanges();
                }
                return tblStudent.iStudentID;
            }
            return 0;
        }

        //Remove
        public void removeStudentByID(int iStudentID)
        {
            tblStudents tblStudent = db.tblStudents.Find(iStudentID);
            if (tblStudent != null)
            {
                tblStudent.bIsDeleted = true;
                db.Entry(tblStudent).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfStudentExists(int iStudentID)
        {
            bool bStudentExists = db.tblStudents.Any(Student => Student.iStudentID == iStudentID && Student.bIsDeleted == false);
            return bStudentExists;
        }

        //Convert database table to class
        public clsStudents convertStudentsTableToClass(tblStudents tblStudent)
        {
            clsStudents clsStudent = new clsStudents();

            clsStudent.iStudentID = tblStudent.iStudentID;
            clsStudent.dtAdded = tblStudent.dtAdded;
            clsStudent.iAddedBy = tblStudent.iAddedBy;
            clsStudent.dtEdited = tblStudent.dtEdited;
            clsStudent.iEditedBy = tblStudent.iEditedBy;

            clsStudent.bIsPaidSubscription = tblStudent.bIsPaidSubscription;
            clsStudent.dtDateOfBirth = tblStudent.dtDateOfBirth;
            clsStudent.strGender = tblStudent.strGender;
            clsStudent.strNationality = tblStudent.strNationality;
            clsStudent.strRegion = tblStudent.strRegion;

            clsStudent.bHigherEducation = tblStudent.bHigherEducation;
            clsStudent.strInstitutionName = tblStudent.strInstitutionName;
            clsStudent.strCourse = tblStudent.strCourse;
            clsStudent.strModules = tblStudent.strModules;

            clsStudent.bIsDeleted = tblStudent.bIsDeleted;

            return clsStudent;
        }
    }
}
