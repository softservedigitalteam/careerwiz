﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsCareersManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsCareers> getAllCareersList()
        {
            List<clsCareers> lstCareers = new List<clsCareers>();
            var lstGetCareersList = db.tblCareers.Where(Career => Career.bIsDeleted == false).ToList();

            if (lstGetCareersList.Count > 0)
            {
                //CMS User Access Manager
                clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();

                foreach (var item in lstGetCareersList)
                {
                    clsCareers clsCareer = new clsCareers();

                    clsCareer.iCareerID = item.iCareerID;
                    clsCareer.dtAdded = item.dtAdded;
                    clsCareer.iAddedBy = item.iAddedBy;
                    clsCareer.dtEdited = item.dtEdited;
                    clsCareer.iEditedBy = item.iEditedBy;

                    clsCareer.strTitle = item.strTitle;
                    clsCareer.strDescription = item.strDescription;
                    clsCareer.strExpectations = item.strExpectations;
                    clsCareer.strRequiredSkills = item.strRequiredSkills;
                    clsCareer.strAverageSalary = item.strAverageSalary;

                    clsCareer.bIsDeleted = item.bIsDeleted;

                    clsCareer.lstIndustrySectorCareerLinkTable = new List<clsIndustrySectorCareerLinkTable>();

                    if (item.tblIndustrySectorCareerLinkTable.Count > 0)
                    {
                        foreach (var IndustrySectorCareerLinkTableItem in item.tblIndustrySectorCareerLinkTable)
                        {
                            clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.convertIndustrySectorCareerLinkTableTableToClass(IndustrySectorCareerLinkTableItem);
                            clsCareer.lstIndustrySectorCareerLinkTable.Add(clsIndustrySectorCareerLinkTable);
                        }
                    }

                    lstCareers.Add(clsCareer);
                }
            }

            return lstCareers;
        }

        //Get
        public clsCareers getCareerByID(int iCareerID)
        {
            clsCareers clsCareer = null;
            tblCareers tblCareers = db.tblCareers.FirstOrDefault(Career => Career.iCareerID == iCareerID && Career.bIsDeleted == false);

            if (tblCareers != null)
            {
                //CMS User Access Manager
                clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();

                clsCareer = new clsCareers();

                clsCareer.iCareerID = tblCareers.iCareerID;
                clsCareer.dtAdded = tblCareers.dtAdded;
                clsCareer.iAddedBy = tblCareers.iAddedBy;
                clsCareer.dtEdited = tblCareers.dtEdited;
                clsCareer.iEditedBy = tblCareers.iEditedBy;

                clsCareer.strTitle = tblCareers.strTitle;
                clsCareer.strDescription = tblCareers.strDescription;
                clsCareer.strExpectations = tblCareers.strExpectations;
                clsCareer.strRequiredSkills = tblCareers.strRequiredSkills;
                clsCareer.strAverageSalary = tblCareers.strAverageSalary;

                clsCareer.bIsDeleted = tblCareers.bIsDeleted;

                clsCareer.lstIndustrySectorCareerLinkTable = new List<clsIndustrySectorCareerLinkTable>();

                if (tblCareers.tblIndustrySectorCareerLinkTable.Count > 0)
                {
                    foreach (var IndustrySectorCareerLinkTableItem in tblCareers.tblIndustrySectorCareerLinkTable)
                    {
                        clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = clsIndustrySectorCareerLinkTableManager.convertIndustrySectorCareerLinkTableTableToClass(IndustrySectorCareerLinkTableItem);
                        clsCareer.lstIndustrySectorCareerLinkTable.Add(clsIndustrySectorCareerLinkTable);
                    }
                }
            }

            return clsCareer;
        }

        //Save
        public void saveCareer(clsCareers clsCareer)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblCareers tblCareers = new tblCareers();

                tblCareers.iCareerID = clsCareer.iCareerID;

                tblCareers.strTitle = clsCareer.strTitle;
                tblCareers.strDescription = clsCareer.strDescription;
                tblCareers.strExpectations = clsCareer.strExpectations;
                tblCareers.strRequiredSkills = clsCareer.strRequiredSkills;
                tblCareers.strAverageSalary = clsCareer.strAverageSalary;

                tblCareers.bIsDeleted = clsCareer.bIsDeleted;

                //Add
                if (tblCareers.iCareerID == 0)
                {
                    tblCareers.dtAdded = DateTime.Now;
                    tblCareers.iAddedBy = clsCMSUser.iCMSUserID;
                    tblCareers.dtEdited = DateTime.Now;
                    tblCareers.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblCareers.Add(tblCareers);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblCareers.dtAdded = clsCareer.dtAdded;
                    tblCareers.iAddedBy = clsCareer.iAddedBy;
                    tblCareers.dtEdited = DateTime.Now;
                    tblCareers.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblCareers>().AddOrUpdate(tblCareers);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeCareerByID(int iCareerID)
        {
            tblCareers tblCareer = db.tblCareers.Find(iCareerID);
            if (tblCareer != null)
            {
                tblCareer.bIsDeleted = true;
                db.Entry(tblCareer).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfCareerExists(int iCareerID)
        {
            bool bCareerExists = db.tblCareers.Any(Career => Career.iCareerID == iCareerID && Career.bIsDeleted == false);
            return bCareerExists;
        }

        //Convert database table to class
        public clsCareers convertCareersTableToClass(tblCareers tblCareer)
        {
            clsCareers clsCareer = new clsCareers();

            clsCareer.iCareerID = tblCareer.iCareerID;
            clsCareer.dtAdded = tblCareer.dtAdded;
            clsCareer.iAddedBy = tblCareer.iAddedBy;
            clsCareer.dtEdited = tblCareer.dtEdited;
            clsCareer.iEditedBy = tblCareer.iEditedBy;

            clsCareer.strTitle = tblCareer.strTitle;
            clsCareer.strDescription = tblCareer.strDescription;
            clsCareer.strExpectations = tblCareer.strExpectations;
            clsCareer.strRequiredSkills = tblCareer.strRequiredSkills;
            clsCareer.strAverageSalary = tblCareer.strAverageSalary;

            clsCareer.bIsDeleted = tblCareer.bIsDeleted;

            return clsCareer;
        }
    }
}
