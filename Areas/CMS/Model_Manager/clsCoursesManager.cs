﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsCoursesManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsCourses> getAllCoursesList()
        {
            List<clsCourses> lstCourses = new List<clsCourses>();
            var lstGetCoursesList = db.tblCourses.Where(Course => Course.bIsDeleted == false).ToList();

            if (lstGetCoursesList.Count > 0)
            {
                //CMS User Access Manager
                clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();

                foreach (var item in lstGetCoursesList)
                {
                    clsCourses clsCourse = new clsCourses();

                    clsCourse.iCourseID = item.iCourseID;
                    clsCourse.dtAdded = item.dtAdded;
                    clsCourse.iAddedBy = item.iAddedBy;
                    clsCourse.dtEdited = item.dtEdited;
                    clsCourse.iEditedBy = item.iEditedBy;

                    clsCourse.strTitle = item.strTitle;
                    clsCourse.strDescription = item.strDescription;
                    clsCourse.bIsDeleted = item.bIsDeleted;

                    clsCourse.lstInstitutionCourseLinkTable = new List<clsInstitutionCourseLinkTable>();

                    if (item.tblInstitutionCourseLinkTable.Count > 0)
                    {
                        foreach (var InstitutionCourseLinkTableItem in item.tblInstitutionCourseLinkTable)
                        {
                            clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.convertInstitutionCourseLinkTableTableToClass(InstitutionCourseLinkTableItem);
                            clsCourse.lstInstitutionCourseLinkTable.Add(clsInstitutionCourseLinkTable);
                        }
                    }

                    lstCourses.Add(clsCourse);
                }
            }

            return lstCourses;
        }

        //Get
        public clsCourses getCourseByID(int iCourseID)
        {
            clsCourses clsCourse = null;
            tblCourses tblCourses = db.tblCourses.FirstOrDefault(Course => Course.iCourseID == iCourseID && Course.bIsDeleted == false);

            if (tblCourses != null)
            {
                //CMS User Access Manager
                clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();

                clsCourse = new clsCourses();

                clsCourse.iCourseID = tblCourses.iCourseID;
                clsCourse.dtAdded = tblCourses.dtAdded;
                clsCourse.iAddedBy = tblCourses.iAddedBy;
                clsCourse.dtEdited = tblCourses.dtEdited;
                clsCourse.iEditedBy = tblCourses.iEditedBy;

                clsCourse.strTitle = tblCourses.strTitle;
                clsCourse.strDescription = tblCourses.strDescription;
                clsCourse.bIsDeleted = tblCourses.bIsDeleted;

                clsCourse.lstInstitutionCourseLinkTable = new List<clsInstitutionCourseLinkTable>();

                if (tblCourses.tblInstitutionCourseLinkTable.Count > 0)
                {
                    foreach (var InstitutionCourseLinkTableItem in tblCourses.tblInstitutionCourseLinkTable)
                    {
                        clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.convertInstitutionCourseLinkTableTableToClass(InstitutionCourseLinkTableItem);
                        clsCourse.lstInstitutionCourseLinkTable.Add(clsInstitutionCourseLinkTable);
                    }
                }
            }

            return clsCourse;
        }

        //Save
        public void saveCourse(clsCourses clsCourse)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblCourses tblCourses = new tblCourses();

                tblCourses.iCourseID = clsCourse.iCourseID;

                tblCourses.strTitle = clsCourse.strTitle;
                tblCourses.strDescription = clsCourse.strDescription;
                tblCourses.bIsDeleted = clsCourse.bIsDeleted;

                //Add
                if (tblCourses.iCourseID == 0)
                {
                    tblCourses.dtAdded = DateTime.Now;
                    tblCourses.iAddedBy = clsCMSUser.iCMSUserID;
                    tblCourses.dtEdited = DateTime.Now;
                    tblCourses.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblCourses.Add(tblCourses);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblCourses.dtAdded = clsCourse.dtAdded;
                    tblCourses.iAddedBy = clsCourse.iAddedBy;
                    tblCourses.dtEdited = DateTime.Now;
                    tblCourses.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblCourses>().AddOrUpdate(tblCourses);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeCourseByID(int iCourseID)
        {
            tblCourses tblCourse = db.tblCourses.Find(iCourseID);
            if (tblCourse != null)
            {
                tblCourse.bIsDeleted = true;
                db.Entry(tblCourse).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfCourseExists(int iCourseID)
        {
            bool bCourseExists = db.tblCourses.Any(Course => Course.iCourseID == iCourseID && Course.bIsDeleted == false);
            return bCourseExists;
        }

        //Convert database table to class
        public clsCourses convertCoursesTableToClass(tblCourses tblCourse)
        {
            clsCourses clsCourse = new clsCourses();

            clsCourse.iCourseID = tblCourse.iCourseID;
            clsCourse.dtAdded = tblCourse.dtAdded;
            clsCourse.iAddedBy = tblCourse.iAddedBy;
            clsCourse.dtEdited = tblCourse.dtEdited;
            clsCourse.iEditedBy = tblCourse.iEditedBy;

            clsCourse.strTitle = tblCourse.strTitle;
            clsCourse.strDescription = tblCourse.strDescription;
            clsCourse.bIsDeleted = tblCourse.bIsDeleted;

            return clsCourse;
        }
    }
}
