﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsInstitutionCourseLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsInstitutionCourseLinkTable> getAllInstitutionCourseLinkTableList()
        {
            List<clsInstitutionCourseLinkTable> lstInstitutionCourseLinkTable = new List<clsInstitutionCourseLinkTable>();
            var lstGetInstitutionCourseLinkTableList = db.tblInstitutionCourseLinkTable.Where(InstitutionCourseLinkTable => InstitutionCourseLinkTable.bIsDeleted == false).ToList();

            if (lstGetInstitutionCourseLinkTableList.Count > 0)
            {
                clsCoursesManager clsCoursesManager = new clsCoursesManager();
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager();

                foreach (var item in lstGetInstitutionCourseLinkTableList)
                {
                    clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();

                    clsInstitutionCourseLinkTable.iInstitutionCourseLinkTableID = item.iInstitutionCourseLinkTableID;
                    clsInstitutionCourseLinkTable.iInstitutionID = item.iInstitutionID;
                    clsInstitutionCourseLinkTable.iCourseID = item.iCourseID;

                    clsInstitutionCourseLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblCourses != null)
                        clsInstitutionCourseLinkTable.clsCourse = clsCoursesManager.convertCoursesTableToClass(item.tblCourses);
                    if (item.tblInstitutions != null)
                        clsInstitutionCourseLinkTable.clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(item.tblInstitutions);

                    lstInstitutionCourseLinkTable.Add(clsInstitutionCourseLinkTable);
                }
            }

            return lstInstitutionCourseLinkTable;
        }

        //Get
        public clsInstitutionCourseLinkTable getInstitutionCourseLinkTableByID(int iInstitutionCourseLinkTableID)
        {
            clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = null;
            tblInstitutionCourseLinkTable tblInstitutionCourseLinkTable = db.tblInstitutionCourseLinkTable.FirstOrDefault(InstitutionCourseLinkTable => InstitutionCourseLinkTable.iInstitutionCourseLinkTableID == iInstitutionCourseLinkTableID && InstitutionCourseLinkTable.bIsDeleted == false);

            if (tblInstitutionCourseLinkTable != null)
            {
                clsCoursesManager clsCoursesManager = new clsCoursesManager(); //Course Manager
                clsInstitutionsManager clsInstitutionsManager = new clsInstitutionsManager(); //Institutions Manger

                clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();

                clsInstitutionCourseLinkTable.iInstitutionCourseLinkTableID = tblInstitutionCourseLinkTable.iInstitutionCourseLinkTableID;
                clsInstitutionCourseLinkTable.iInstitutionID = tblInstitutionCourseLinkTable.iInstitutionID;
                clsInstitutionCourseLinkTable.iCourseID = tblInstitutionCourseLinkTable.iCourseID;

                clsInstitutionCourseLinkTable.bIsDeleted = tblInstitutionCourseLinkTable.bIsDeleted;

                if (tblInstitutionCourseLinkTable.tblCourses != null)
                    clsInstitutionCourseLinkTable.clsCourse = clsCoursesManager.convertCoursesTableToClass(tblInstitutionCourseLinkTable.tblCourses);
                if (tblInstitutionCourseLinkTable.tblInstitutions != null)
                    clsInstitutionCourseLinkTable.clsInstitution = clsInstitutionsManager.convertInstitutionsTableToClass(tblInstitutionCourseLinkTable.tblInstitutions);
            }

            return clsInstitutionCourseLinkTable;
        }

        //Save
        public void saveInstitutionCourseLinkTable(clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblInstitutionCourseLinkTable tblInstitutionCourseLinkTable = new tblInstitutionCourseLinkTable();

                tblInstitutionCourseLinkTable.iInstitutionCourseLinkTableID = clsInstitutionCourseLinkTable.iInstitutionCourseLinkTableID;

                tblInstitutionCourseLinkTable.iCourseID = clsInstitutionCourseLinkTable.iCourseID;
                tblInstitutionCourseLinkTable.iInstitutionID = clsInstitutionCourseLinkTable.iInstitutionID;
                tblInstitutionCourseLinkTable.iCourseID = clsInstitutionCourseLinkTable.iCourseID;
                tblInstitutionCourseLinkTable.bIsDeleted = clsInstitutionCourseLinkTable.bIsDeleted;

                //Add
                if (tblInstitutionCourseLinkTable.iInstitutionCourseLinkTableID == 0)
                {
                    db.tblInstitutionCourseLinkTable.Add(tblInstitutionCourseLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblInstitutionCourseLinkTable>().AddOrUpdate(tblInstitutionCourseLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeInstitutionCourseLinkTableByID(int iInstitutionCourseLinkTableID)
        {
            tblInstitutionCourseLinkTable tblInstitutionCourseLinkTable = db.tblInstitutionCourseLinkTable.Find(iInstitutionCourseLinkTableID);
            if (tblInstitutionCourseLinkTable != null)
            {
                tblInstitutionCourseLinkTable.bIsDeleted = true;
                db.Entry(tblInstitutionCourseLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfInstitutionCourseLinkTableExists(int iInstitutionCourseLinkTableID)
        {
            bool bInstitutionCourseLinkTableExists = db.tblInstitutionCourseLinkTable.Any(InstitutionCourseLinkTable => InstitutionCourseLinkTable.iInstitutionCourseLinkTableID == iInstitutionCourseLinkTableID && InstitutionCourseLinkTable.bIsDeleted == false);
            return bInstitutionCourseLinkTableExists;
        }


        //Convert database table to class
        public clsInstitutionCourseLinkTable convertInstitutionCourseLinkTableTableToClass(tblInstitutionCourseLinkTable tblInstitutionCourseLinkTable)
        {
            clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();

            clsInstitutionCourseLinkTable.iInstitutionCourseLinkTableID = tblInstitutionCourseLinkTable.iInstitutionCourseLinkTableID;

            clsInstitutionCourseLinkTable.iCourseID = tblInstitutionCourseLinkTable.iCourseID;
            clsInstitutionCourseLinkTable.iInstitutionID = tblInstitutionCourseLinkTable.iInstitutionID;

            clsInstitutionCourseLinkTable.bIsDeleted = tblInstitutionCourseLinkTable.bIsDeleted;

            return clsInstitutionCourseLinkTable;
        }
    }
}
