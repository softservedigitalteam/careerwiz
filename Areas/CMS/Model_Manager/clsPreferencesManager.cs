﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsPreferencesManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsPreferences> getAllPreferencesList()
        {
            List<clsPreferences> lstPreferences = new List<clsPreferences>();
            var lstGetPreferencesList = db.tblPreferences.Where(Preference => Preference.bIsDeleted == false).ToList();

            if (lstGetPreferencesList.Count > 0)
            {
                clsOpportunityPreferenceLinkTableManager clsOpportunityPreferenceLinkTableManager = new clsOpportunityPreferenceLinkTableManager();
                clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();

                foreach (var item in lstGetPreferencesList)
                {
                    clsPreferences clsPreference = new clsPreferences();

                    clsPreference.iPreferenceID = item.iPreferenceID;
                    clsPreference.dtAdded = item.dtAdded;
                    clsPreference.iAddedBy = item.iAddedBy;
                    clsPreference.dtEdited = item.dtEdited;
                    clsPreference.iEditedBy = item.iEditedBy;

                    clsPreference.strTitle = item.strTitle;
                    clsPreference.bIsDeleted = item.bIsDeleted;

                    clsPreference.lstOpportunityPreferenceLinkTable = new List<clsOpportunityPreferenceLinkTable>();
                    clsPreference.lstUserPreferenceLinkTable = new List<clsUserPreferenceLinkTable>();

                    if (item.tblOpportunityPreferenceLinkTable.Count > 0)
                    {
                        foreach (var OpportunityPreferenceLinkTableItem in item.tblOpportunityPreferenceLinkTable)
                        {
                            clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = clsOpportunityPreferenceLinkTableManager.convertOpportunityPreferenceLinkTableTableToClass(OpportunityPreferenceLinkTableItem);
                            clsPreference.lstOpportunityPreferenceLinkTable.Add(clsOpportunityPreferenceLinkTable);
                        }
                    }
                    if (item.tblUserPreferenceLinkTable.Count > 0)
                    {
                        foreach (var UserPreferenceLinkTableItem in item.tblUserPreferenceLinkTable)
                        {
                            clsUserPreferenceLinkTable clsUserPreferenceLinkTable = clsUserPreferenceLinkTableManager.convertUserPreferenceLinkTableTableToClass(UserPreferenceLinkTableItem);
                            clsPreference.lstUserPreferenceLinkTable.Add(clsUserPreferenceLinkTable);
                        }
                    }

                    lstPreferences.Add(clsPreference);
                }
            }

            return lstPreferences;
        }

        //Get
        public clsPreferences getPreferenceByID(int iPreferenceID)
        {
            clsPreferences clsPreference = null;
            tblPreferences tblPreference = db.tblPreferences.FirstOrDefault(Preference => Preference.iPreferenceID == iPreferenceID && Preference.bIsDeleted == false);

            if (tblPreference != null)
            {
                clsOpportunityPreferenceLinkTableManager clsOpportunityPreferenceLinkTableManager = new clsOpportunityPreferenceLinkTableManager();
                clsUserPreferenceLinkTableManager clsUserPreferenceLinkTableManager = new clsUserPreferenceLinkTableManager();

                clsPreference = new clsPreferences();
                clsPreference.iPreferenceID = tblPreference.iPreferenceID;
                clsPreference.dtAdded = tblPreference.dtAdded;
                clsPreference.iAddedBy = tblPreference.iAddedBy;
                clsPreference.dtEdited = tblPreference.dtEdited;
                clsPreference.iEditedBy = tblPreference.iEditedBy;

                clsPreference.strTitle = tblPreference.strTitle;
                clsPreference.bIsDeleted = tblPreference.bIsDeleted;

                clsPreference.lstOpportunityPreferenceLinkTable = new List<clsOpportunityPreferenceLinkTable>();

                if (tblPreference.tblOpportunityPreferenceLinkTable.Count > 0)
                {
                    foreach (var OpportunityPreferenceLinkTableItem in tblPreference.tblOpportunityPreferenceLinkTable)
                    {
                        clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable = clsOpportunityPreferenceLinkTableManager.convertOpportunityPreferenceLinkTableTableToClass(OpportunityPreferenceLinkTableItem);
                        clsPreference.lstOpportunityPreferenceLinkTable.Add(clsOpportunityPreferenceLinkTable);
                    }
                }
                if (tblPreference.tblUserPreferenceLinkTable.Count > 0)
                {
                    foreach (var UserPreferenceLinkTableItem in tblPreference.tblUserPreferenceLinkTable)
                    {
                        clsUserPreferenceLinkTable clsUserPreferenceLinkTable = clsUserPreferenceLinkTableManager.convertUserPreferenceLinkTableTableToClass(UserPreferenceLinkTableItem);
                        clsPreference.lstUserPreferenceLinkTable.Add(clsUserPreferenceLinkTable);
                    }
                }
            }
            return clsPreference;
        }

        //Save
        public void savePreference(clsPreferences clsPreference)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblPreferences tblPreference = new tblPreferences();

                tblPreference.iPreferenceID = clsPreference.iPreferenceID;

                tblPreference.strTitle = clsPreference.strTitle;

                tblPreference.bIsDeleted = clsPreference.bIsDeleted;

                //Add
                if (tblPreference.iPreferenceID == 0)
                {
                    tblPreference.dtAdded = DateTime.Now;
                    tblPreference.iAddedBy = clsCMSUser.iCMSUserID;
                    tblPreference.dtEdited = DateTime.Now;
                    tblPreference.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblPreferences.Add(tblPreference);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblPreference.dtAdded = clsPreference.dtAdded;
                    tblPreference.iAddedBy = clsPreference.iAddedBy;
                    tblPreference.dtEdited = DateTime.Now;
                    tblPreference.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblPreferences>().AddOrUpdate(tblPreference);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removePreferenceByID(int iPreferenceID)
        {
            tblPreferences tblPreference = db.tblPreferences.Find(iPreferenceID);
            if (tblPreference != null)
            {
                tblPreference.bIsDeleted = true;
                db.Entry(tblPreference).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfPreferenceExists(int iPreferenceID)
        {
            bool bPreferenceExists = db.tblPreferences.Any(Preference => Preference.iPreferenceID == iPreferenceID && Preference.bIsDeleted == false);
            return bPreferenceExists;
        }

        //Convert database table to class
        public clsPreferences convertPreferencesTableToClass(tblPreferences tblPreference)
        {
            clsPreferences clsPreference = new clsPreferences();

            clsPreference.iPreferenceID = tblPreference.iPreferenceID;
            clsPreference.dtAdded = tblPreference.dtAdded;
            clsPreference.iAddedBy = tblPreference.iAddedBy;
            clsPreference.dtEdited = tblPreference.dtEdited;
            clsPreference.iEditedBy = tblPreference.iEditedBy;

            clsPreference.strTitle = tblPreference.strTitle;

            clsPreference.bIsDeleted = tblPreference.bIsDeleted;

            return clsPreference;
        }
    }
}
