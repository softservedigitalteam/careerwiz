﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsInstitutionsManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsInstitutions> getAllInstitutionsList()
        {
            List<clsInstitutions> lstInstitutions = new List<clsInstitutions>();
            var lstGetInstitutionsList = db.tblInstitutions.Where(Institution => Institution.bIsDeleted == false).ToList();

            if (lstGetInstitutionsList.Count > 0)
            {
                clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
                clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
                clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();

                foreach (var item in lstGetInstitutionsList)
                {
                    clsInstitutions clsInstitution = new clsInstitutions();

                    clsInstitution.iInstitutionID = item.iInstitutionID;
                    clsInstitution.dtAdded = item.dtAdded;
                    clsInstitution.iAddedBy = item.iAddedBy;
                    clsInstitution.dtEdited = item.dtEdited;
                    clsInstitution.iEditedBy = item.iEditedBy;

                    clsInstitution.strTitle = item.strTitle;
                    clsInstitution.strDescription = item.strDescription;
                    clsInstitution.strDescriptionOverview = item.strDescriptionOverview;
                    clsInstitution.iInstitutionTypeID = item.iInstitutionTypeID;
                    clsInstitution.strContactDetails = item.strContactDetails;

                    clsInstitution.strVisionMission = item.strVisionMission;
                    clsInstitution.strCampusLife = item.strCampusLife;
                    clsInstitution.strImportantDatesInformation = item.strImportantDatesInformation;
                    clsInstitution.strInstitutionURL = item.strInstitutionURL;
                    clsInstitution.strImagePath = item.strImagePath;

                    clsInstitution.strImageName = item.strImageName;
                    clsInstitution.bIsDeleted = item.bIsDeleted;

                    clsInstitution.lstInstitutionCourseLinkTable = new List<clsInstitutionCourseLinkTable>();
                    clsInstitution.lstInstitutionIndustrySectorLinkTable = new List<clsInstitutionIndustrySectorLinkTable>();

                    if (item.tblInstitutionCourseLinkTable.Count > 0)
                    {
                        foreach (var InstitutionCourseLinkTableItem in item.tblInstitutionCourseLinkTable)
                        {
                            clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.convertInstitutionCourseLinkTableTableToClass(InstitutionCourseLinkTableItem);
                            clsInstitution.lstInstitutionCourseLinkTable.Add(clsInstitutionCourseLinkTable);
                        }
                    }
                    if (item.tblInstitutionIndustrySectorLinkTable.Count > 0)
                    {
                        foreach (var InstitutionIndustrySectorLinkTableItem in item.tblInstitutionIndustrySectorLinkTable)
                        {
                            clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.convertInstitutionIndustrySectorLinkTableTableToClass(InstitutionIndustrySectorLinkTableItem);
                            clsInstitution.lstInstitutionIndustrySectorLinkTable.Add(clsInstitutionIndustrySectorLinkTable);
                        }
                    }
                    if (item.tblInstitutionTypes != null)
                        clsInstitution.clsInstitutionType = clsInstitutionTypesManager.convertInstitutionTypesTableToClass(item.tblInstitutionTypes);

                    lstInstitutions.Add(clsInstitution);
                }
            }

            return lstInstitutions;
        }

        //Get
        public clsInstitutions getInstitutionByID(int iInstitutionID)
        {
            clsInstitutions clsInstitution = null;
            tblInstitutions tblInstitution = db.tblInstitutions.FirstOrDefault(Institution => Institution.iInstitutionID == iInstitutionID && Institution.bIsDeleted == false);

            if (tblInstitution != null)
            {
                clsInstitutionCourseLinkTableManager clsInstitutionCourseLinkTableManager = new clsInstitutionCourseLinkTableManager();
                clsInstitutionIndustrySectorLinkTableManager clsInstitutionIndustrySectorLinkTableManager = new clsInstitutionIndustrySectorLinkTableManager();
                clsInstitutionTypesManager clsInstitutionTypesManager = new clsInstitutionTypesManager();

                clsInstitution = new clsInstitutions();
                clsInstitution.iInstitutionID = tblInstitution.iInstitutionID;
                clsInstitution.dtAdded = tblInstitution.dtAdded;
                clsInstitution.iAddedBy = tblInstitution.iAddedBy;
                clsInstitution.dtEdited = tblInstitution.dtEdited;
                clsInstitution.iEditedBy = tblInstitution.iEditedBy;

                clsInstitution.strTitle = tblInstitution.strTitle;
                clsInstitution.strDescription = tblInstitution.strDescription;
                clsInstitution.strDescriptionOverview = tblInstitution.strDescriptionOverview;
                clsInstitution.iInstitutionTypeID = tblInstitution.iInstitutionTypeID;
                clsInstitution.strContactDetails = tblInstitution.strContactDetails;

                clsInstitution.strVisionMission = tblInstitution.strVisionMission;
                clsInstitution.strCampusLife = tblInstitution.strCampusLife;
                clsInstitution.strImportantDatesInformation = tblInstitution.strImportantDatesInformation;
                clsInstitution.strInstitutionURL = tblInstitution.strInstitutionURL;
                clsInstitution.strImagePath = tblInstitution.strImagePath;

                clsInstitution.strImageName = tblInstitution.strImageName;
                clsInstitution.bIsDeleted = tblInstitution.bIsDeleted;

                clsInstitution.lstInstitutionCourseLinkTable = new List<clsInstitutionCourseLinkTable>();

                if (tblInstitution.tblInstitutionCourseLinkTable.Count > 0)
                {
                    foreach (var InstitutionCourseLinkTableItem in tblInstitution.tblInstitutionCourseLinkTable)
                    {
                        clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable = clsInstitutionCourseLinkTableManager.convertInstitutionCourseLinkTableTableToClass(InstitutionCourseLinkTableItem);
                        clsInstitution.lstInstitutionCourseLinkTable.Add(clsInstitutionCourseLinkTable);
                    }
                }
                if (tblInstitution.tblInstitutionIndustrySectorLinkTable.Count > 0)
                {
                    foreach (var InstitutionIndustrySectorLinkTableItem in tblInstitution.tblInstitutionIndustrySectorLinkTable)
                    {
                        clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable = clsInstitutionIndustrySectorLinkTableManager.convertInstitutionIndustrySectorLinkTableTableToClass(InstitutionIndustrySectorLinkTableItem);
                        clsInstitution.lstInstitutionIndustrySectorLinkTable.Add(clsInstitutionIndustrySectorLinkTable);
                    }
                }
                if (tblInstitution.tblInstitutionTypes != null)
                    clsInstitution.clsInstitutionType = clsInstitutionTypesManager.convertInstitutionTypesTableToClass(tblInstitution.tblInstitutionTypes);
            }
            return clsInstitution;
        }

        //Save
        public void saveInstitution(clsInstitutions clsInstitution)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblInstitutions tblInstitution = new tblInstitutions();

                tblInstitution.iInstitutionID = clsInstitution.iInstitutionID;

                tblInstitution.strTitle = clsInstitution.strTitle;
                tblInstitution.strDescription = clsInstitution.strDescription;
                tblInstitution.strDescriptionOverview = clsInstitution.strDescriptionOverview;
                tblInstitution.iInstitutionTypeID = clsInstitution.iInstitutionTypeID;
                tblInstitution.strContactDetails = clsInstitution.strContactDetails;

                tblInstitution.strVisionMission = clsInstitution.strVisionMission;
                tblInstitution.strCampusLife = clsInstitution.strCampusLife;
                tblInstitution.strImportantDatesInformation = clsInstitution.strImportantDatesInformation;
                tblInstitution.strInstitutionURL = clsInstitution.strInstitutionURL;
                tblInstitution.strImagePath = clsInstitution.strImagePath;

                tblInstitution.strImageName = clsInstitution.strImageName;
                tblInstitution.bIsDeleted = clsInstitution.bIsDeleted;

                //Add
                if (tblInstitution.iInstitutionID == 0)
                {
                    tblInstitution.dtAdded = DateTime.Now;
                    tblInstitution.iAddedBy = clsCMSUser.iCMSUserID;
                    tblInstitution.dtEdited = DateTime.Now;
                    tblInstitution.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblInstitutions.Add(tblInstitution);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblInstitution.dtAdded = clsInstitution.dtAdded;
                    tblInstitution.iAddedBy = clsInstitution.iAddedBy;
                    tblInstitution.dtEdited = DateTime.Now;
                    tblInstitution.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblInstitutions>().AddOrUpdate(tblInstitution);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeInstitutionByID(int iInstitutionID)
        {
            tblInstitutions tblInstitution = db.tblInstitutions.Find(iInstitutionID);
            if (tblInstitution != null)
            {
                tblInstitution.bIsDeleted = true;
                db.Entry(tblInstitution).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfInstitutionExists(int iInstitutionID)
        {
            bool bInstitutionExists = db.tblInstitutions.Any(Institution => Institution.iInstitutionID == iInstitutionID && Institution.bIsDeleted == false);
            return bInstitutionExists;
        }

        //Convert database table to class
        public clsInstitutions convertInstitutionsTableToClass(tblInstitutions tblInstitution)
        {
            clsInstitutions clsInstitution = new clsInstitutions();

            clsInstitution.iInstitutionID = tblInstitution.iInstitutionID;
            clsInstitution.dtAdded = tblInstitution.dtAdded;
            clsInstitution.iAddedBy = tblInstitution.iAddedBy;
            clsInstitution.dtEdited = tblInstitution.dtEdited;
            clsInstitution.iEditedBy = tblInstitution.iEditedBy;

            clsInstitution.strTitle = tblInstitution.strTitle;
            clsInstitution.strDescription = tblInstitution.strDescription;
            clsInstitution.strDescriptionOverview = tblInstitution.strDescriptionOverview;
            clsInstitution.iInstitutionTypeID = tblInstitution.iInstitutionTypeID;
            clsInstitution.strContactDetails = tblInstitution.strContactDetails;

            clsInstitution.strVisionMission = tblInstitution.strVisionMission;
            clsInstitution.strCampusLife = tblInstitution.strCampusLife;
            clsInstitution.strImportantDatesInformation = tblInstitution.strImportantDatesInformation;
            clsInstitution.strInstitutionURL = tblInstitution.strInstitutionURL;
            clsInstitution.strImagePath = tblInstitution.strImagePath;

            clsInstitution.strImageName = tblInstitution.strImageName;
            clsInstitution.bIsDeleted = tblInstitution.bIsDeleted;

            return clsInstitution;
        }
    }
}
