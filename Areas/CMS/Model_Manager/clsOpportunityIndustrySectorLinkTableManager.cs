﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;


namespace CareerWizCMS.Model_Manager
{
    public class clsOpportunityIndustrySectorLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsOpportunityIndustrySectorLinkTable> getAllOpportunityIndustrySectorLinkTableList()
        {
            List<clsOpportunityIndustrySectorLinkTable> lstOpportunityIndustrySectorLinkTable = new List<clsOpportunityIndustrySectorLinkTable>();
            var lstGetOpportunityIndustrySectorLinkTableList = db.tblOpportunityIndustrySectorLinkTable.Where(OpportunityIndustrySectorLinkTable => OpportunityIndustrySectorLinkTable.bIsDeleted == false).ToList();

            if (lstGetOpportunityIndustrySectorLinkTableList.Count > 0)
            {
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager();
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager();

                foreach (var item in lstGetOpportunityIndustrySectorLinkTableList)
                {
                    clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();

                    clsOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID = item.iOpportunityIndustrySectorLinkTableID;
                    clsOpportunityIndustrySectorLinkTable.iOpportunityID = item.iOpportunityID;
                    clsOpportunityIndustrySectorLinkTable.iIndustrySectorID = item.iIndustrySectorID;

                    clsOpportunityIndustrySectorLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblIndustrySectors != null)
                        clsOpportunityIndustrySectorLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(item.tblIndustrySectors);
                    if (item.tblOpportunities != null)
                        clsOpportunityIndustrySectorLinkTable.clsOpportunity = clsOpportunitiesManager.convertOpportunitiesTableToClass(item.tblOpportunities);

                    lstOpportunityIndustrySectorLinkTable.Add(clsOpportunityIndustrySectorLinkTable);
                }
            }

            return lstOpportunityIndustrySectorLinkTable;
        }

        //Get
        public clsOpportunityIndustrySectorLinkTable getOpportunityIndustrySectorLinkTableByID(int iOpportunityIndustrySectorLinkTableID)
        {
            clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = null;
            tblOpportunityIndustrySectorLinkTable tblOpportunityIndustrySectorLinkTable = db.tblOpportunityIndustrySectorLinkTable.FirstOrDefault(OpportunityIndustrySectorLinkTable => OpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID == iOpportunityIndustrySectorLinkTableID && OpportunityIndustrySectorLinkTable.bIsDeleted == false);

            if (tblOpportunityIndustrySectorLinkTable != null)
            {
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager(); //IndustrySector Manager
                clsOpportunitiesManager clsOpportunitiesManager = new clsOpportunitiesManager(); //Opportunities Manger

                clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();

                clsOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID = tblOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID;
                clsOpportunityIndustrySectorLinkTable.iOpportunityID = tblOpportunityIndustrySectorLinkTable.iOpportunityID;
                clsOpportunityIndustrySectorLinkTable.iIndustrySectorID = tblOpportunityIndustrySectorLinkTable.iIndustrySectorID;

                clsOpportunityIndustrySectorLinkTable.bIsDeleted = tblOpportunityIndustrySectorLinkTable.bIsDeleted;

                if (tblOpportunityIndustrySectorLinkTable.tblIndustrySectors != null)
                    clsOpportunityIndustrySectorLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(tblOpportunityIndustrySectorLinkTable.tblIndustrySectors);
                if (tblOpportunityIndustrySectorLinkTable.tblOpportunities != null)
                    clsOpportunityIndustrySectorLinkTable.clsOpportunity = clsOpportunitiesManager.convertOpportunitiesTableToClass(tblOpportunityIndustrySectorLinkTable.tblOpportunities);
            }

            return clsOpportunityIndustrySectorLinkTable;
        }

        //Save
        public void saveOpportunityIndustrySectorLinkTable(clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblOpportunityIndustrySectorLinkTable tblOpportunityIndustrySectorLinkTable = new tblOpportunityIndustrySectorLinkTable();

                tblOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID = clsOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID;

                tblOpportunityIndustrySectorLinkTable.iIndustrySectorID = clsOpportunityIndustrySectorLinkTable.iIndustrySectorID;
                tblOpportunityIndustrySectorLinkTable.iOpportunityID = clsOpportunityIndustrySectorLinkTable.iOpportunityID;
                tblOpportunityIndustrySectorLinkTable.iIndustrySectorID = clsOpportunityIndustrySectorLinkTable.iIndustrySectorID;
                tblOpportunityIndustrySectorLinkTable.bIsDeleted = clsOpportunityIndustrySectorLinkTable.bIsDeleted;

                //Add
                if (tblOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID == 0)
                {
                    db.tblOpportunityIndustrySectorLinkTable.Add(tblOpportunityIndustrySectorLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblOpportunityIndustrySectorLinkTable>().AddOrUpdate(tblOpportunityIndustrySectorLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeOpportunityIndustrySectorLinkTableByID(int iOpportunityIndustrySectorLinkTableID)
        {
            tblOpportunityIndustrySectorLinkTable tblOpportunityIndustrySectorLinkTable = db.tblOpportunityIndustrySectorLinkTable.Find(iOpportunityIndustrySectorLinkTableID);
            if (tblOpportunityIndustrySectorLinkTable != null)
            {
                tblOpportunityIndustrySectorLinkTable.bIsDeleted = true;
                db.Entry(tblOpportunityIndustrySectorLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfOpportunityIndustrySectorLinkTableExists(int iOpportunityIndustrySectorLinkTableID)
        {
            bool bOpportunityIndustrySectorLinkTableExists = db.tblOpportunityIndustrySectorLinkTable.Any(OpportunityIndustrySectorLinkTable => OpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID == iOpportunityIndustrySectorLinkTableID && OpportunityIndustrySectorLinkTable.bIsDeleted == false);
            return bOpportunityIndustrySectorLinkTableExists;
        }


        //Convert database table to class
        public clsOpportunityIndustrySectorLinkTable convertOpportunityIndustrySectorLinkTableTableToClass(tblOpportunityIndustrySectorLinkTable tblOpportunityIndustrySectorLinkTable)
        {
            clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();

            clsOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID = tblOpportunityIndustrySectorLinkTable.iOpportunityIndustrySectorLinkTableID;

            clsOpportunityIndustrySectorLinkTable.iIndustrySectorID = tblOpportunityIndustrySectorLinkTable.iIndustrySectorID;
            clsOpportunityIndustrySectorLinkTable.iOpportunityID = tblOpportunityIndustrySectorLinkTable.iOpportunityID;

            clsOpportunityIndustrySectorLinkTable.bIsDeleted = tblOpportunityIndustrySectorLinkTable.bIsDeleted;

            return clsOpportunityIndustrySectorLinkTable;
        }
    }
}
