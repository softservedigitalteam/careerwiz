﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWiz;
using CareerWizCMS.Models;

namespace CareerWizCMS.Model_Manager
{
    public class clsUserPreferenceLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsUserPreferenceLinkTable> getAllUserPreferenceLinkTableList()
        {
            List<clsUserPreferenceLinkTable> lstUserPreferenceLinkTable = new List<clsUserPreferenceLinkTable>();
            var lstGetUserPreferenceLinkTableList = db.tblUserPreferenceLinkTable.Where(UserPreferenceLinkTable => UserPreferenceLinkTable.bIsDeleted == false).ToList();

            if (lstGetUserPreferenceLinkTableList.Count > 0)
            {
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager();
                clsUsersManager clsUsersManager = new clsUsersManager();

                foreach (var item in lstGetUserPreferenceLinkTableList)
                {
                    clsUserPreferenceLinkTable clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();

                    clsUserPreferenceLinkTable.iUserPreferenceLinkTableID = item.iUserPreferenceLinkTableID;
                    clsUserPreferenceLinkTable.iUserID = item.iUserID;
                    clsUserPreferenceLinkTable.iPreferenceID = item.iPreferenceID;

                    clsUserPreferenceLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblPreferences != null)
                        clsUserPreferenceLinkTable.clsPreference = clsPreferencesManager.convertPreferencesTableToClass(item.tblPreferences);
                    if (item.tblUsers != null)
                        clsUserPreferenceLinkTable.clsUser = clsUsersManager.convertUsersTableToClass(item.tblUsers);

                    lstUserPreferenceLinkTable.Add(clsUserPreferenceLinkTable);
                }
            }

            return lstUserPreferenceLinkTable;
        }

        //Get
        public clsUserPreferenceLinkTable getUserPreferenceLinkTableByID(int iUserPreferenceLinkTableID)
        {
            clsUserPreferenceLinkTable clsUserPreferenceLinkTable = null;
            tblUserPreferenceLinkTable tblUserPreferenceLinkTable = db.tblUserPreferenceLinkTable.FirstOrDefault(UserPreferenceLinkTable => UserPreferenceLinkTable.iUserPreferenceLinkTableID == iUserPreferenceLinkTableID && UserPreferenceLinkTable.bIsDeleted == false);

            if (tblUserPreferenceLinkTable != null)
            {
                clsPreferencesManager clsPreferencesManager = new clsPreferencesManager(); //Preference Manager
                clsUsersManager clsUsersManager = new clsUsersManager(); //Users Manger

                clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();

                clsUserPreferenceLinkTable.iUserPreferenceLinkTableID = tblUserPreferenceLinkTable.iUserPreferenceLinkTableID;
                clsUserPreferenceLinkTable.iUserID = tblUserPreferenceLinkTable.iUserID;
                clsUserPreferenceLinkTable.iPreferenceID = tblUserPreferenceLinkTable.iPreferenceID;

                clsUserPreferenceLinkTable.bIsDeleted = tblUserPreferenceLinkTable.bIsDeleted;

                if (tblUserPreferenceLinkTable.tblPreferences != null)
                    clsUserPreferenceLinkTable.clsPreference = clsPreferencesManager.convertPreferencesTableToClass(tblUserPreferenceLinkTable.tblPreferences);
                if (tblUserPreferenceLinkTable.tblUsers != null)
                    clsUserPreferenceLinkTable.clsUser = clsUsersManager.convertUsersTableToClass(tblUserPreferenceLinkTable.tblUsers);
            }

            return clsUserPreferenceLinkTable;
        }

        //Save
        public void saveUserPreferenceLinkTable(clsUserPreferenceLinkTable clsUserPreferenceLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblUserPreferenceLinkTable tblUserPreferenceLinkTable = new tblUserPreferenceLinkTable();

                tblUserPreferenceLinkTable.iUserPreferenceLinkTableID = clsUserPreferenceLinkTable.iUserPreferenceLinkTableID;

                tblUserPreferenceLinkTable.iPreferenceID = clsUserPreferenceLinkTable.iPreferenceID;
                tblUserPreferenceLinkTable.iUserID = clsUserPreferenceLinkTable.iUserID;
                tblUserPreferenceLinkTable.iPreferenceID = clsUserPreferenceLinkTable.iPreferenceID;
                tblUserPreferenceLinkTable.bIsDeleted = clsUserPreferenceLinkTable.bIsDeleted;

                //Add
                if (tblUserPreferenceLinkTable.iUserPreferenceLinkTableID == 0)
                {
                    db.tblUserPreferenceLinkTable.Add(tblUserPreferenceLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblUserPreferenceLinkTable>().AddOrUpdate(tblUserPreferenceLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeUserPreferenceLinkTableByID(int iUserPreferenceLinkTableID)
        {
            tblUserPreferenceLinkTable tblUserPreferenceLinkTable = db.tblUserPreferenceLinkTable.Find(iUserPreferenceLinkTableID);
            if (tblUserPreferenceLinkTable != null)
            {
                tblUserPreferenceLinkTable.bIsDeleted = true;
                db.Entry(tblUserPreferenceLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfUserPreferenceLinkTableExists(int iUserPreferenceLinkTableID)
        {
            bool bUserPreferenceLinkTableExists = db.tblUserPreferenceLinkTable.Any(UserPreferenceLinkTable => UserPreferenceLinkTable.iUserPreferenceLinkTableID == iUserPreferenceLinkTableID && UserPreferenceLinkTable.bIsDeleted == false);
            return bUserPreferenceLinkTableExists;
        }


        //Convert database table to class
        public clsUserPreferenceLinkTable convertUserPreferenceLinkTableTableToClass(tblUserPreferenceLinkTable tblUserPreferenceLinkTable)
        {
            clsUserPreferenceLinkTable clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();

            clsUserPreferenceLinkTable.iUserPreferenceLinkTableID = tblUserPreferenceLinkTable.iUserPreferenceLinkTableID;

            clsUserPreferenceLinkTable.iPreferenceID = tblUserPreferenceLinkTable.iPreferenceID;
            clsUserPreferenceLinkTable.iUserID = tblUserPreferenceLinkTable.iUserID;

            clsUserPreferenceLinkTable.bIsDeleted = tblUserPreferenceLinkTable.bIsDeleted;

            return clsUserPreferenceLinkTable;
        }
    }
}
