﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CareerWizCMS.Models;
using CareerWiz;

namespace CareerWizCMS.Model_Manager
{
    public class clsIndustrySectorCareerLinkTableManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        //Get All
        public List<clsIndustrySectorCareerLinkTable> getAllIndustrySectorCareerLinkTableList()
        {
            List<clsIndustrySectorCareerLinkTable> lstIndustrySectorCareerLinkTable = new List<clsIndustrySectorCareerLinkTable>();
            var lstGetIndustrySectorCareerLinkTableList = db.tblIndustrySectorCareerLinkTable.Where(IndustrySectorCareerLinkTable => IndustrySectorCareerLinkTable.bIsDeleted == false).ToList();

            if (lstGetIndustrySectorCareerLinkTableList.Count > 0)
            {
                clsCareersManager clsCareersManager = new clsCareersManager(); //CMS User Manager
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager(); //CMS Pages Manger

                foreach (var item in lstGetIndustrySectorCareerLinkTableList)
                {
                    clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();

                    clsIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID = item.iIndustrySectorCareerLinkTableID;

                    clsIndustrySectorCareerLinkTable.iCareerID = item.iCareerID;
                    clsIndustrySectorCareerLinkTable.iIndustrySectorID = item.iIndustrySectorID;
                    clsIndustrySectorCareerLinkTable.bIsDeleted = item.bIsDeleted;

                    if (item.tblCareers != null)
                        clsIndustrySectorCareerLinkTable.clsCareer = clsCareersManager.convertCareersTableToClass(item.tblCareers);
                    if (item.tblIndustrySectors != null)
                        clsIndustrySectorCareerLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(item.tblIndustrySectors);

                    lstIndustrySectorCareerLinkTable.Add(clsIndustrySectorCareerLinkTable);
                }
            }

            return lstIndustrySectorCareerLinkTable;
        }

        //Get
        public clsIndustrySectorCareerLinkTable getIndustrySectorCareerLinkTableByID(int iIndustrySectorCareerLinkTableID)
        {
            clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = null;
            tblIndustrySectorCareerLinkTable tblIndustrySectorCareerLinkTable = db.tblIndustrySectorCareerLinkTable.FirstOrDefault(IndustrySectorCareerLinkTable => IndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID == iIndustrySectorCareerLinkTableID && IndustrySectorCareerLinkTable.bIsDeleted == false);

            if (tblIndustrySectorCareerLinkTable != null)
            {
                clsCareersManager clsCareersManager = new clsCareersManager(); //CMS User Manager
                clsIndustrySectorsManager clsIndustrySectorsManager = new clsIndustrySectorsManager(); //CMS Pages Manger

                clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();

                clsIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID = tblIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID;

                clsIndustrySectorCareerLinkTable.iCareerID = tblIndustrySectorCareerLinkTable.iCareerID;
                clsIndustrySectorCareerLinkTable.iIndustrySectorID = tblIndustrySectorCareerLinkTable.iIndustrySectorID;
                clsIndustrySectorCareerLinkTable.bIsDeleted = tblIndustrySectorCareerLinkTable.bIsDeleted;

                if (tblIndustrySectorCareerLinkTable.tblCareers != null)
                    clsIndustrySectorCareerLinkTable.clsCareer = clsCareersManager.convertCareersTableToClass(tblIndustrySectorCareerLinkTable.tblCareers);
                if (tblIndustrySectorCareerLinkTable.tblIndustrySectors != null)
                    clsIndustrySectorCareerLinkTable.clsIndustrySector = clsIndustrySectorsManager.convertIndustrySectorsTableToClass(tblIndustrySectorCareerLinkTable.tblIndustrySectors);
            }

            return clsIndustrySectorCareerLinkTable;
        }

        //Save
        public void saveIndustrySectorCareerLinkTable(clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblIndustrySectorCareerLinkTable tblIndustrySectorCareerLinkTable = new tblIndustrySectorCareerLinkTable();

                tblIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID = clsIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID;

                tblIndustrySectorCareerLinkTable.iCareerID = clsIndustrySectorCareerLinkTable.iCareerID;
                tblIndustrySectorCareerLinkTable.iIndustrySectorID = clsIndustrySectorCareerLinkTable.iIndustrySectorID;
                tblIndustrySectorCareerLinkTable.bIsDeleted = clsIndustrySectorCareerLinkTable.bIsDeleted;

                //Add
                if (tblIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID == 0)
                {
                    db.tblIndustrySectorCareerLinkTable.Add(tblIndustrySectorCareerLinkTable);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    db.Set<tblIndustrySectorCareerLinkTable>().AddOrUpdate(tblIndustrySectorCareerLinkTable);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeIndustrySectorCareerLinkTableByID(int iIndustrySectorCareerLinkTableID)
        {
            tblIndustrySectorCareerLinkTable tblIndustrySectorCareerLinkTable = db.tblIndustrySectorCareerLinkTable.Find(iIndustrySectorCareerLinkTableID);
            if (tblIndustrySectorCareerLinkTable != null)
            {
                tblIndustrySectorCareerLinkTable.bIsDeleted = true;
                db.Entry(tblIndustrySectorCareerLinkTable).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfIndustrySectorCareerLinkTableExists(int iIndustrySectorCareerLinkTableID)
        {
            bool bIndustrySectorCareerLinkTableExists = db.tblIndustrySectorCareerLinkTable.Any(IndustrySectorCareerLinkTable => IndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID == iIndustrySectorCareerLinkTableID && IndustrySectorCareerLinkTable.bIsDeleted == false);
            return bIndustrySectorCareerLinkTableExists;
        }

        //Convert database table to class
        public clsIndustrySectorCareerLinkTable convertIndustrySectorCareerLinkTableTableToClass(tblIndustrySectorCareerLinkTable tblIndustrySectorCareerLinkTable)
        {
            clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();

            clsIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID = tblIndustrySectorCareerLinkTable.iIndustrySectorCareerLinkTableID;

            clsIndustrySectorCareerLinkTable.iCareerID = tblIndustrySectorCareerLinkTable.iCareerID;
            clsIndustrySectorCareerLinkTable.iIndustrySectorID = tblIndustrySectorCareerLinkTable.iIndustrySectorID;
            clsIndustrySectorCareerLinkTable.bIsDeleted = tblIndustrySectorCareerLinkTable.bIsDeleted;

            return clsIndustrySectorCareerLinkTable;
        }
    }
}
