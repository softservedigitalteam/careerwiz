﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.CMSUserAccess
{
    public class clsCMSUserAccessView
    {
        public List<clsCMSUserAccess> lstCMSUserAccess { get; set; }
        public int iCMSUserAccessID { get; set; }
    }
}
