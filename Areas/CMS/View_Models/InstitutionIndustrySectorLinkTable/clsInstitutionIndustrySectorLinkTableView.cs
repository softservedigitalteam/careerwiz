﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.InstitutionIndustrySectorLinkTable
{
    public class clsInstitutionIndustrySectorLinkTableView
    {
        public List<clsInstitutionIndustrySectorLinkTable> lstInstitutionIndustrySectorLinkTable { get; set; }
        public int iInstitutionIndustrySectorLinkTableID { get; set; }
    }
}
