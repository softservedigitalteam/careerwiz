﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.InstitutionIndustrySectorLinkTable
{
    public class clsInstitutionIndustrySectorLinkTableAdd
    {
        public clsInstitutionIndustrySectorLinkTableAdd()
        {
            clsInstitutionIndustrySectorLinkTable = new clsInstitutionIndustrySectorLinkTable();
        }
        public clsInstitutionIndustrySectorLinkTable clsInstitutionIndustrySectorLinkTable { get; set; }
        public List<clsInstitutions> lstInstitutions { get; set; }
        public List<clsIndustrySectors> lstIndustrySectors { get; set; }

    }
}
