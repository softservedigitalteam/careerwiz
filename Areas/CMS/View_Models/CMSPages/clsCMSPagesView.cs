﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.CMSPages
{
    public class clsCMSPagesView
    {
        public List<clsCMSPages> lstCMSPages { get; set; }
        public int iCMSPageID { get; set; }
    }
}
