﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Preferences
{
    public class clsPreferencesView
    {
        public List<clsPreferences> lstPreferences { get; set; }
        public int iPreferenceID { get; set; }
    }
}
