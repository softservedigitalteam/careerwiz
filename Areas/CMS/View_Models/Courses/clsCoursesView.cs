﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Courses
{
    public class clsCoursesView
    {
        public List<clsCourses> lstCourses { get; set; }
        public int iCourseID { get; set; }
    }
}
