﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Courses
{
    public class clsCourseEdit
    {
        public clsCourseEdit()
        {
            clsCourse = new clsCourses();
        }
        public clsCourses clsCourse { get; set; }
        public string strTitle { get; set; }
    }
}
