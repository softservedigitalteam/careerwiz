﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Opportunities
{
    public class clsOpportunitiesView
    {
        public List<clsOpportunities> lstOpportunities { get; set; }
        public int iOpportunityID { get; set; }
    }
}
