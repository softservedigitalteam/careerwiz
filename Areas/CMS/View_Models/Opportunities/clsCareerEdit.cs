﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Opportunities
{
    public class clsOpportunityEdit
    {
        public clsOpportunityEdit()
        {
            clsOpportunity = new clsOpportunities();
        }
        public clsOpportunities clsOpportunity { get; set; }
        public string strTitle { get; set; }
    }
}
