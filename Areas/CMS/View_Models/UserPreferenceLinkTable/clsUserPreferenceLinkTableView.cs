﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.UserPreferenceLinkTable
{
    public class clsUserPreferenceLinkTableView
    {
        public List<clsUserPreferenceLinkTable> lstUserPreferenceLinkTable { get; set; }
        public int iUserPreferenceLinkTableID { get; set; }
    }
}
