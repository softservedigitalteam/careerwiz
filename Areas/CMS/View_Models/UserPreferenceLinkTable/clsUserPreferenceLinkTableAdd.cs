﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.UserPreferenceLinkTable
{
    public class clsUserPreferenceLinkTableAdd
    {
        public clsUserPreferenceLinkTableAdd()
        {
            clsUserPreferenceLinkTable = new clsUserPreferenceLinkTable();
        }
        public clsUserPreferenceLinkTable clsUserPreferenceLinkTable { get; set; }
        public List<clsUsers> lstUsers { get; set; }
        public List<clsPreferences> lstPreferences { get; set; }

    }
}
