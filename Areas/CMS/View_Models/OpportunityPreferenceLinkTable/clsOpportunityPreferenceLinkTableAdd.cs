﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.OpportunityPreferenceLinkTable
{
    public class clsOpportunityPreferenceLinkTableAdd
    {
        public clsOpportunityPreferenceLinkTableAdd()
        {
            clsOpportunityPreferenceLinkTable = new clsOpportunityPreferenceLinkTable();
        }
        public clsOpportunityPreferenceLinkTable clsOpportunityPreferenceLinkTable { get; set; }
        public List<clsOpportunities> lstOpportunities { get; set; }
        public List<clsPreferences> lstPreferences { get; set; }

    }
}
