﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.OpportunityPreferenceLinkTable
{
    public class clsOpportunityPreferenceLinkTableView
    {
        public List<clsOpportunityPreferenceLinkTable> lstOpportunityPreferenceLinkTable { get; set; }
        public int iOpportunityPreferenceLinkTableID { get; set; }
    }
}
