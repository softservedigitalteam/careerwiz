﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.IndustrySectorCareerLinkTable
{
    public class clsIndustrySectorCareerLinkTableAdd
    {
        public clsIndustrySectorCareerLinkTableAdd()
        {
            clsIndustrySectorCareerLinkTable = new clsIndustrySectorCareerLinkTable();
        }
        public clsIndustrySectorCareerLinkTable clsIndustrySectorCareerLinkTable { get; set; }
        public List<clsIndustrySectors> lstIndustrySectors { get; set; }
        public List<clsCareers> lstCareers { get; set; }

    }
}
