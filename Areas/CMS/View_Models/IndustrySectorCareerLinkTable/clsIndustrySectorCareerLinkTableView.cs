﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.IndustrySectorCareerLinkTable
{
    public class clsIndustrySectorCareerLinkTableView
    {
        public List<clsIndustrySectorCareerLinkTable> lstIndustrySectorCareerLinkTable { get; set; }
        public int iIndustrySectorCareerLinkTableID { get; set; }
    }
}
