﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.UserAccess
{
    public class clsUserAccessView
    {
        public List<clsUserAccess> lstUserAccess { get; set; }
        public int iUserAccessID { get; set; }
    }
}
