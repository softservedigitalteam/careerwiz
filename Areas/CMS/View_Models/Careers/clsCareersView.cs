﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Careers
{
    public class clsCareersView
    {
        public List<clsCareers> lstCareers { get; set; }
        public int iCareerID { get; set; }
    }
}
