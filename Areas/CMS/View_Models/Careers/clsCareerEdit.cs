﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Careers
{
    public class clsCareerEdit
    {
        public clsCareerEdit()
        {
            clsCareer = new clsCareers();
        }
        public clsCareers clsCareer { get; set; }
        public string strTitle { get; set; }
    }
}
