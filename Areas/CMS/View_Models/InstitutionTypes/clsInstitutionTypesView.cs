﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.InstitutionTypes
{
    public class clsInstitutionTypesView
    {
        public List<clsInstitutionTypes> lstInstitutionTypes { get; set; }
        public int iInstitutionTypeID { get; set; }
    }
}
