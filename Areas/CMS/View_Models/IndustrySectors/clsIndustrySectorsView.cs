﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.IndustrySectors
{
    public class clsIndustrySectorsView
    {
        public List<clsIndustrySectors> lstIndustrySectors { get; set; }
        public int iIndustrySectorID { get; set; }
    }
}
