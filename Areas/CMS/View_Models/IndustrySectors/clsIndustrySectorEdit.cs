﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.IndustrySectors
{
    public class clsIndustrySectorEdit
    {
        public clsIndustrySectorEdit()
        {
            clsIndustrySector = new clsIndustrySectors();
        }
        public clsIndustrySectors clsIndustrySector { get; set; }
        public string strTitle { get; set; }
    }
}
