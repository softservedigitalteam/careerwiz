﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Users
{
    public class clsUserAdd
    {
        public clsUserAdd()
        {
            clsUser = new clsUsers();
            clsUser.clsStudent = new clsStudents();
        }
        public clsUsers clsUser { get; set; }
        public List<clsRoleTypes> lstRoleTypes { get; set; }

        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be at least 6 characters long")]
        public string strNewPassword { get; set; }
       
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be at least 6 characters long")]
        [Compare("strNewPassword", ErrorMessage = "Password does not match")]
        public string strConfirmPassword { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [Range(1, 31, ErrorMessage = "DD must be between 1 and 31")]
        public int? iDateOfBirthDay { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [Range(1, 12, ErrorMessage = "MM must be between 1 and 12")]
        public int? iDateOfBirthMonth { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [Range(1900, 9999, ErrorMessage = "Incorrect YYYY value")]
        public int? iDateOfBirthYear { get; set; }

        public string strCropImageData { get; set; }
        public string strCropImageName { get; set; }
    }
}
