﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.CMSRoleTypes
{
    public class clsCMSRoleTypesView
    {
        public List<clsCMSRoleTypes> lstCMSRoleTypes { get; set; }
        public int iCMSRoleTypeID { get; set; }
    }
}
