﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.InstitutionCourseLinkTable
{
    public class clsInstitutionCourseLinkTableView
    {
        public List<clsInstitutionCourseLinkTable> lstInstitutionCourseLinkTable { get; set; }
        public int iInstitutionCourseLinkTableID { get; set; }
    }
}
