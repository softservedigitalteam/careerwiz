﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.InstitutionCourseLinkTable
{
    public class clsInstitutionCourseLinkTableAdd
    {
        public clsInstitutionCourseLinkTableAdd()
        {
            clsInstitutionCourseLinkTable = new clsInstitutionCourseLinkTable();
        }
        public clsInstitutionCourseLinkTable clsInstitutionCourseLinkTable { get; set; }
        public List<clsInstitutions> lstInstitutions { get; set; }
        public List<clsCourses> lstCourses { get; set; }

    }
}
