﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.OpportunityIndustrySectorLinkTable
{
    public class clsOpportunityIndustrySectorLinkTableEdit
    {
        public clsOpportunityIndustrySectorLinkTableEdit()
        {
            clsOpportunityIndustrySectorLinkTable = new clsOpportunityIndustrySectorLinkTable();
        }
        public clsOpportunityIndustrySectorLinkTable clsOpportunityIndustrySectorLinkTable { get; set; }
        public List<clsOpportunities> lstOpportunities { get; set; }
        public List<clsIndustrySectors> lstIndustrySectors { get; set; }

    }
}
