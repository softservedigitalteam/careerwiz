﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.OpportunityIndustrySectorLinkTable
{
    public class clsOpportunityIndustrySectorLinkTableView
    {
        public List<clsOpportunityIndustrySectorLinkTable> lstOpportunityIndustrySectorLinkTable { get; set; }
        public int iOpportunityIndustrySectorLinkTableID { get; set; }
    }
}
