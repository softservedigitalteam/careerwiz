﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CareerWizCMS.Model_Manager;
using CareerWizCMS.Models;
using CareerWizCMS.View_Models;
using CareerWizCMS.View_Models.Users;

namespace CareerWizCMS.View_Models.CMSUsers
{
    public class clsCMSUsersView
    {
        public List<clsCMSUsers> lstCMSUsers { get; set; }
        public int iCMSUserID { get; set; }
    }
}
