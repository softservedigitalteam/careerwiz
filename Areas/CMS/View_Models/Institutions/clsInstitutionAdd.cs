﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Institutions
{
    public class clsInstitutionsAdd
    {
        public clsInstitutionsAdd()
        {
            clsInstitution = new clsInstitutions();
        }
        public clsInstitutions clsInstitution { get; set; }
        public List<clsInstitutionTypes> lstInstitutionTypes { get; set; }
        public string strCropImageData { get; set; }
        public string strCropImageName { get; set; }
    }
}
