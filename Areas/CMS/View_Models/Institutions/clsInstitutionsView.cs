﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareerWizCMS.Models;

namespace CareerWizCMS.View_Models.Institutions
{
    public class clsInstitutionsView
    {
        public List<clsInstitutions> lstInstitutions { get; set; }
        public int iInstitutionID { get; set; }
    }
}
