﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CareerWiz.Startup))]
namespace CareerWiz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
