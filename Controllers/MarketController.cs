﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWiz.Models;
using CareerWiz.Model_Manager;
using CareerWizCMS.Model_Manager;
using PagedList.Mvc;
using PagedList;

namespace CareerWiz.Controllers
{
    public class MarketController : Controller
    {
        CareerWizDBContext db = new CareerWizDBContext();
        // GET: Market
        public ActionResult Marketplace()
        {
            return View();
        }


        public ActionResult Institutions(int? page)
        {
            int ipageSize = 5;
            int ipageIndex = 1;
            ipageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<clsInstitutions> institutions = null;
            clsInstitutions clsInstitutions = new clsInstitutions();
            List<clsInstitutions> lstInstitutions = (List<clsInstitutions>)TempData["clsInstitutionList"];
            if (lstInstitutions == null)
            {
                if (HttpContext.Request.Cookies["hdnPreference"] != null)
                {
                    HttpCookie cookie = HttpContext.Request.Cookies.Get("hdnPreference"); //Get cookie
                    var iPrefID = Convert.ToInt32(cookie.Value);
                    Model_Manager.clsInstitutionsManager clsInstitutionsManager = new Model_Manager.clsInstitutionsManager();
                    lstInstitutions = clsInstitutionsManager.getInstitutionsByTypeID(iPrefID);
                }

            }
            institutions = lstInstitutions.ToPagedList(ipageIndex, ipageSize);
            return View(institutions);
        }

        public ActionResult Opportunities()
        {
            return View();
        }

        public ActionResult Market_information()
        {
            clsIndustries clsIndustries = new clsIndustries();
            clsIndustrySectors clsIndustrySectors = new clsIndustrySectors();
            clsIndustrySectors.lstIndustryList = new SelectList(clsIndustries.GetIndustrySectors(), "iIndustrySectorID", "strTitle");
            return View(clsIndustrySectors);
        }

        [HttpGet]
        public ActionResult Career_Options(clsCareers model)
        {
            clsCareers clsCareer = (clsCareers)TempData["clsCareer"];
            return View(clsCareer);
        }

        [HttpPost]
        public void CareerInfo(int iIndustrySectorID)
        {
            clsIndustrySectorCareerLinkTableManager clsIndustrySectorCareerLinkTableManager = new clsIndustrySectorCareerLinkTableManager();
            clsCareers clsCareers = new clsCareers();
            List<CareerWizCMS.Models.clsIndustrySectorCareerLinkTable> list = clsIndustrySectorCareerLinkTableManager.getAllIndustrySectorCareerLinkTableList().Where(x => x.iIndustrySectorID == iIndustrySectorID).ToList();
            clsCareers.lstCareerList = new SelectList(list, "clsCareer.iCareerID", "clsCareer.strTitle");
            TempData["clsCareer"] = clsCareers;
            RedirectToAction("Career_Options", "Market");
        }

        [HttpPost]
        public void getCareerExpectations(int iCareerID)
        {
            clsCareerManager clsCareerManager = new clsCareerManager();
            clsCareers clsCareers = new clsCareers();
            clsCareers = clsCareerManager.getCareerByID(iCareerID);
            TempData["clsCareer"] = clsCareers;
            RedirectToAction("Career_Expectations", "Market");
        }

        [HttpGet]
        public ActionResult Career_Expectations()
        {
            clsCareers clsCareer = (clsCareers)TempData["clsCareer"];
            return View(clsCareer);
        }


        public ActionResult Institution_Preference()
        {
            return View();
        }

        [HttpPost]
        public void Test(int iPreferenceID)
        {
            Model_Manager.clsInstitutionsManager clsInstitutionsManager = new Model_Manager.clsInstitutionsManager();
            List<clsInstitutions> lstInstitutions = clsInstitutionsManager.getInstitutionsByTypeID(iPreferenceID);
            TempData["clsInstitutionList"] = lstInstitutions;
            RedirectToAction("Institutions", "Market");
        }
    }
}