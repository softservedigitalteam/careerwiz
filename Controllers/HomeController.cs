﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CareerWiz.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _PartialSlider()
        {
            return PartialView();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Discover()
        {
            return View();
        }
    }
}