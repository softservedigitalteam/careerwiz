﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CareerWiz.Models.User;
using CareerWizCMS.Model_Manager;
using CareerWizCMS.Models;
using CareerWizCMS.Assistant_Classes;

namespace CareerWiz.Controllers
{
    public class LoginController : Controller
    {
        [Authorize]
        // GET: Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            CareerWizDBContext db = new CareerWizDBContext();
            clsUserLogin clslogin = new clsUserLogin();
            HttpCookie hcUserLoginCookie = Request.Cookies["strUserLoginEmail"];
            if(hcUserLoginCookie != null)
            {
                clslogin.strEmail = hcUserLoginCookie.Value;
                clslogin.bRememberMe = true;
            }

            return View();
        }

        // POST: Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(clsUserLogin clslogin)
        {
            clsUsersManager clsUsersManager = new clsUsersManager();
            clsUsers clsUser = clsUsersManager.getUserByEmail(clslogin.strEmail.ToLower());
            if (clsUser == null)
            {
                ModelState.AddModelError("loginError", "Incorrect Login Details Entered");
                clslogin.strPassword = "";
                return View(clslogin);
            }

            string strPasswordhash = "";
            if (clslogin.strPassword != null && clslogin.strPassword != "")
                strPasswordhash = clsCommonFunctions.GetMd5Sum(clslogin.strPassword);

            //If password is incorrect
            if(clsUser.strPassword != strPasswordhash)
            {
                ModelState.AddModelError("loginError", "Incorrect Login Details Entered");
                clslogin.strPassword = "";
                return View(clslogin);
            }
            else
            {
                //set cookie
                if(clslogin.bRememberMe)
                {
                    HttpCookie hcUserLoginCookie = new HttpCookie("strUserLoginEmail");
                    hcUserLoginCookie.Value = clslogin.strEmail;
                    hcUserLoginCookie.Expires = DateTime.Now.AddDays(30);
                    HttpContext.Response.Cookies.Add(hcUserLoginCookie);
                }
                else
                {
                    if(Request.Cookies["strUserLoginEmail"] != null)
                    {
                        Response.Cookies["strUserLoginEmail"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                ModelState.AddModelError("loginError", "");
                Session["clsUserLogin"] = clsUser;
                return RedirectToAction("Index", "Home");
            }

        }


    }
}