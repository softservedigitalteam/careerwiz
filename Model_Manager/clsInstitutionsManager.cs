﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareerWiz.Models;

namespace CareerWiz.Model_Manager
{
    public class clsInstitutionsManager
    {
        CareerWizDBContext db = new CareerWizDBContext();
        //Get full list of institutions
        public List<clsInstitutions> getListOfInstitutions()
        {
            List<clsInstitutions> lstInstitutions = new List<clsInstitutions>();
            var lstInstitutionsList = db.tblInstitutions.Where(institution => institution.bIsDeleted == false).ToList();
            if(lstInstitutionsList.Count > 0)
            {
                foreach(var item in lstInstitutionsList)
                {
                    clsInstitutions clsInstitutions = new clsInstitutions();
                    clsInstitutions.iInstitutionID = item.iInstitutionID;
                    clsInstitutions.iInstitutionTypeID = item.iInstitutionTypeID;
                    clsInstitutions.dtAdded = item.dtAdded;
                    clsInstitutions.iAddedBy = item.iAddedBy;
                    clsInstitutions.dtEdited = item.dtEdited;
                    clsInstitutions.iEditedBy = item.iEditedBy;
                    clsInstitutions.strTitle = item.strTitle;
                    clsInstitutions.strDescription = item.strDescription;
                    clsInstitutions.strDescriptionOverview = item.strDescriptionOverview;
                    clsInstitutions.strCampusLife = item.strCampusLife;
                    clsInstitutions.strContactDetails = item.strContactDetails;
                    clsInstitutions.strImportantDatesInformation = item.strImportantDatesInformation;
                    clsInstitutions.strInstitutionURL = item.strInstitutionURL;
                    clsInstitutions.strVisionMission = item.strVisionMission;
                    clsInstitutions.bIsDeleted = item.bIsDeleted;
                    lstInstitutions.Add(clsInstitutions);
                }
            }
            return (lstInstitutions);
        }

        //Get institutions by their type iD
        public List<clsInstitutions> getInstitutionsByTypeID(int iInstituteTypeID)
        {
            List<clsInstitutions> lstInstitutionsByTypeID = new List<clsInstitutions>();
            var listInstitutionsListByTypeID = db.tblInstitutions.Where(institution => institution.iInstitutionTypeID.Equals(iInstituteTypeID) && institution.bIsDeleted == false).ToList();

            if(listInstitutionsListByTypeID.Count > 0)
            {
                foreach(var item in listInstitutionsListByTypeID)
                {
                    clsInstitutions clsInstitutions = new clsInstitutions();
                    clsInstitutions.iInstitutionID = item.iInstitutionID;
                    clsInstitutions.iInstitutionTypeID = item.iInstitutionTypeID;
                    clsInstitutions.dtAdded = item.dtAdded;
                    clsInstitutions.iAddedBy = item.iAddedBy;
                    clsInstitutions.dtEdited = item.dtEdited;
                    clsInstitutions.iEditedBy = item.iEditedBy;
                    clsInstitutions.strTitle = item.strTitle;
                    clsInstitutions.strDescription = item.strDescription;
                    clsInstitutions.strDescriptionOverview = item.strDescriptionOverview;
                    clsInstitutions.strCampusLife = item.strCampusLife;
                    clsInstitutions.strContactDetails = item.strContactDetails;
                    clsInstitutions.strImportantDatesInformation = item.strImportantDatesInformation;
                    clsInstitutions.strInstitutionURL = item.strInstitutionURL;
                    clsInstitutions.strVisionMission = item.strVisionMission;
                    clsInstitutions.bIsDeleted = item.bIsDeleted;
                    lstInstitutionsByTypeID.Add(clsInstitutions);
                }
            }
            return (lstInstitutionsByTypeID);
        }
    }
}