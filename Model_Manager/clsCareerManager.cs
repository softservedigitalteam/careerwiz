﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareerWiz.Models;

namespace CareerWiz.Model_Manager
{
    public class clsCareerManager
    {
        CareerWizDBContext db = new CareerWizDBContext();

        public clsCareers getCareerByID(int iCareerID)
        {
            clsCareers clsCareer = null;
            tblCareers tblCareer = db.tblCareers.FirstOrDefault(career => career.iCareerID == iCareerID && career.bIsDeleted == false);
            if(tblCareer != null)
            {
                clsCareer = new clsCareers();
                clsCareer.iCareerID = tblCareer.iCareerID;
                clsCareer.iAddedBy = tblCareer.iAddedBy;
                clsCareer.dtAdded = tblCareer.dtAdded;
                clsCareer.iEditedBy = tblCareer.iEditedBy;
                clsCareer.dtEdited = tblCareer.dtEdited;
                clsCareer.strTitle = tblCareer.strTitle;
                clsCareer.strDescription = tblCareer.strDescription;
                clsCareer.strExpectations = tblCareer.strExpectations;
                clsCareer.strRequiredSkills = tblCareer.strRequiredSkills;
                clsCareer.strAverageSalary = tblCareer.strAverageSalary;
                clsCareer.bIsDeleted = tblCareer.bIsDeleted;
            }
            return (clsCareer);
        }
    }
}