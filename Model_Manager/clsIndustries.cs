﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CareerWiz.Models;

namespace CareerWiz.Model_Manager
{
    public class clsIndustries
    {
        CareerWizDBContext db = new CareerWizDBContext();

        public List<clsIndustrySectors> GetIndustrySectors()
        {
            List<clsIndustrySectors> lstIndustries = new List<clsIndustrySectors>();
            var lstIndustrySectors = db.tblIndustrySectors.Where(IndustrySector => IndustrySector.bIsDeleted == false).ToList();
            if (lstIndustrySectors.Count > 0)
            {
                foreach (var item in lstIndustrySectors)
                {
                    clsIndustrySectors clsIndustrySectors = new clsIndustrySectors();
                    clsIndustrySectors.iIndustrySectorID = item.iIndustrySectorID;
                    clsIndustrySectors.iAddedBy = item.iAddedBy;
                    clsIndustrySectors.dtAdded = item.dtAdded;
                    clsIndustrySectors.iEditedBy = item.iEditedBy;
                    clsIndustrySectors.dtEdited = item.dtEdited;
                    clsIndustrySectors.strTitle = item.strTitle;
                    clsIndustrySectors.strDescription = item.strDescription;
                    clsIndustrySectors.bIsDeleted = item.bIsDeleted;
                    lstIndustries.Add(clsIndustrySectors);
                }
            }
            return (lstIndustries);
        }
    }
}