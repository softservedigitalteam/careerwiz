﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CareerWiz.Models
{
    public class clsInstitutions
    {
        public int iInstitutionID { get; set; }
        public int iAddedBy { get; set; }
        public System.DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public Nullable<System.DateTime> dtEdited { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strDescriptionOverview { get; set; }
        public int iInstitutionTypeID { get; set; }
        public string strContactDetails { get; set; }
        public string strVisionMission { get; set; }
        public string strCampusLife { get; set; }
        public string strImportantDatesInformation { get; set; }
        public string strInstitutionURL { get; set; }
        public bool bIsDeleted { get; set; }
        public List<clsInstitutions> lstInstitutions { get; set; }
        public int hdnValue { get; set; }

    }
}