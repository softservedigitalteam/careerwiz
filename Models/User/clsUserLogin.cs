﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CareerWiz.Models.User
{
    public class clsUserLogin
    {
        public string strEmail { get; set; }

        public string strPassword { get; set; }

        public bool bRememberMe { get; set; }
    }
}