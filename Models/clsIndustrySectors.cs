﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CareerWiz.Models
{
    public class clsIndustrySectors
    {
        public int iIndustrySectorID { get; set; }
        public int iAddedBy { get; set; }
        public System.DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public Nullable<System.DateTime> dtEdited { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public bool bIsDeleted { get; set; }
        public SelectList lstIndustryList { get; set; }
    }
}