﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CareerWiz.Models
{
    public class clsCareers
    {
        public int iCareerID { get; set; }
        public int iAddedBy { get; set; }
        public System.DateTime dtAdded { get; set; }
        public int iEditedBy { get; set; }
        public Nullable<System.DateTime> dtEdited { get; set; }
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strExpectations { get; set; }
        public string strRequiredSkills { get; set; }
        public string strAverageSalary { get; set; }
        public bool bIsDeleted { get; set; }
        public SelectList lstCareerList { get; set; }
    }
}